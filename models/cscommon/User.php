<?php

namespace app\models\cscommon;

use Yii;

/**
 * This is the model class for table "USERS".
 *
 * @property integer $usr_id
 * @property string $usr_name_cyr
 * @property string $usr_name_lat
 * @property string $usr_login
 * @property string $usr_pwd_hash
 * @property integer $usr_is_active
 * @property integer $usr_grp_id
 * @property integer $usr_role_id
 * @property integer $usr_mgr_id
 * @property string $usr_comment
 * @property string $auth_key
 *
 * @property USERALIASES[] $uSERALIASESs
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'USERS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('cscommon');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usr_name_cyr', 'usr_name_lat', 'usr_is_active', 'usr_grp_id', 'usr_role_id', 'auth_key'], 'required'],
            [['usr_is_active', 'usr_grp_id', 'usr_role_id', 'usr_mgr_id'], 'integer'],
            [['usr_name_cyr', 'usr_name_lat', 'usr_login', 'usr_pwd_hash', 'usr_comment'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['usr_name_cyr'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usr_id' => 'Usr ID',
            'usr_name_cyr' => 'Usr Name Cyr',
            'usr_name_lat' => 'Usr Name Lat',
            'usr_login' => 'Usr Login',
            'usr_pwd_hash' => 'Usr Pwd Hash',
            'usr_is_active' => 'Usr Is Active',
            'usr_grp_id' => 'Usr Grp ID',
            'usr_role_id' => 'Usr Role ID',
            'usr_mgr_id' => 'Usr Mgr ID',
            'usr_comment' => 'Usr Comment',
            'auth_key' => 'Auth Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSERALIASESs()
    {
        return $this->hasMany(USERALIASES::className(), ['ua_usr_id' => 'usr_id']);
    }
}
