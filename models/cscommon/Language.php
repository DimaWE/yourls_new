<?php

namespace app\models\cscommon;

use Yii;
use app\models\sqhelp\{SurveyModel};

/**
 * This is the model class for table "LANGUAGES".
 *
 * @property integer $lng_id
 * @property string $lng_code
 * @property string $lng_name
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cscommon.LANGUAGES';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lng_code', 'lng_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lng_id' => 'Lng ID',
            'lng_code' => 'Lng Code',
            'lng_name' => 'Lng Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyModels()
    {
        return $this->hasMany(SurveyModel::className(), ['srv_lng_id' => 'lng_id']);
    }
}
