<?php

namespace app\models\cscommon;

use Yii;
use app\models\sqhelp\{SurveyModel};

/**
 * This is the model class for table "PUBLISHERS".
 *
 * @property integer $pbs_id
 * @property string $pbs_code
 * @property string $pbs_name
 * @property integer $pbs_ptm_id
 *
 * @property PLATFORMS $pbsPtm
 */
class Publisher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cscommon.PUBLISHERS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pbs_ptm_id'], 'integer'],
            [['pbs_code', 'pbs_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pbs_id' => 'Pbs ID',
            'pbs_code' => 'Pbs Code',
            'pbs_name' => 'Pbs Name',
            'pbs_ptm_id' => 'Pbs Ptm ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyModels()
    {
        return $this->hasMany(SurveyModel::className(), ['srv_pbs_id' => 'pbs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['ptm_id' => 'pbs_ptm_id']);
    }
}
