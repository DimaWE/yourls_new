<?php

namespace app\models\cscommon;

use Yii;
use app\models\sqhelp\{SurveyModel};

/**
 * This is the model class for table "PROJECTS".
 *
 * @property integer $pjt_id
 * @property string $pjt_code
 * @property string $pjt_name
 * @property string $pjt_type
 *
 * @property USERALIASES[] $uSERALIASESs
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cscommon.PROJECTS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pjt_code', 'pjt_name', 'pjt_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pjt_id' => 'Pjt ID',
            'pjt_code' => 'Pjt Code',
            'pjt_name' => 'Pjt Name',
            'pjt_type' => 'Pjt Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyModels()
    {
        return $this->hasMany(SurveyModel::className(), ['srv_pjt_id' => 'pjt_id']);
    }
}
