<?php

namespace app\models\cscommon;

use Yii;
use app\models\sqhelp\{SurveyModel};

/**
 * This is the model class for table "CHANNELS".
 *
 * @property integer $cnl_id
 * @property string $cnl_code
 * @property string $cnl_name
 */
class Channel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cscommon.CHANNELS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cnl_code', 'cnl_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cnl_id' => 'Cnl ID',
            'cnl_code' => 'Cnl Code',
            'cnl_name' => 'Cnl Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyModels()
    {
        return $this->hasMany(SurveyModel::className(), ['srv_cnl_id' => 'cnl_id']);
    }
}
