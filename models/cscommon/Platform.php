<?php

namespace app\models\cscommon;

use Yii;

/**
 * This is the model class for table "PLATFORMS".
 *
 * @property integer $ptm_id
 * @property string $ptm_code
 * @property string $ptm_name
 */
class Platform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cscommon.PLATFORMS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ptm_code', 'ptm_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ptm_id' => 'Ptm ID',
            'ptm_code' => 'Ptm Code',
            'ptm_name' => 'Ptm Name',
        ];
    }
}
