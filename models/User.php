<?php

namespace app\models;

use Yii;

class User extends \app\models\cscommon\User implements \yii\web\IdentityInterface
{
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['usr_id' => $id, 'usr_is_active' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['usr_login' => $username, 'usr_is_active' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return (
            sha1($password) == $this->usr_pwd_hash
        );
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function isAdmin()
    {
        return in_array(Yii::$app->user->identity->usr_grp_id, [1, 3, 4, 5, 7]);
    }
}