<?php

namespace app\models\hdr1;

use Yii;

/**
 * This is the model class for table "swkbarticlelinks".
 *
 * @property integer $kbarticlelinkid
 * @property integer $kbarticleid
 * @property integer $linktype
 * @property integer $linktypeid
 */
class Swkbarticlelinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'swkbarticlelinks';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('hdr1');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kbarticleid', 'linktype', 'linktypeid'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kbarticlelinkid' => 'Kbarticlelinkid',
            'kbarticleid' => 'Kbarticleid',
            'linktype' => 'Linktype',
            'linktypeid' => 'Linktypeid',
        ];
    }
}
