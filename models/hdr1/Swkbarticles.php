<?php

namespace app\models\hdr1;

use Yii;

/**
 * This is the model class for table "swkbarticles".
 *
 * @property integer $kbarticleid
 * @property integer $creator
 * @property integer $creatorid
 * @property string $author
 * @property string $email
 * @property string $subject
 * @property integer $isedited
 * @property integer $editeddateline
 * @property integer $editedstaffid
 * @property integer $views
 * @property integer $isfeatured
 * @property integer $allowcomments
 * @property integer $totalcomments
 * @property integer $hasattachments
 * @property integer $dateline
 * @property string $articlestatus
 * @property double $articlerating
 * @property integer $ratinghits
 * @property integer $ratingcount
 * @property string $keywords
 */
class Swkbarticles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'swkbarticles';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('hdr1');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator', 'creatorid', 'isedited', 'editeddateline', 'editedstaffid', 'views', 'isfeatured', 'allowcomments', 'totalcomments', 'hasattachments', 'dateline', 'ratinghits', 'ratingcount'], 'integer'],
            [['articlerating'], 'number'],
            [['author', 'email', 'subject', 'keywords'], 'string', 'max' => 255],
            [['articlestatus'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kbarticleid' => 'Kbarticleid',
            'creator' => 'Creator',
            'creatorid' => 'Creatorid',
            'author' => 'Author',
            'email' => 'Email',
            'subject' => 'Subject',
            'isedited' => 'Isedited',
            'editeddateline' => 'Editeddateline',
            'editedstaffid' => 'Editedstaffid',
            'views' => 'Views',
            'isfeatured' => 'Isfeatured',
            'allowcomments' => 'Allowcomments',
            'totalcomments' => 'Totalcomments',
            'hasattachments' => 'Hasattachments',
            'dateline' => 'Dateline',
            'articlestatus' => 'Articlestatus',
            'articlerating' => 'Articlerating',
            'ratinghits' => 'Ratinghits',
            'ratingcount' => 'Ratingcount',
            'keywords' => 'Keywords',
        ];
    }
}
