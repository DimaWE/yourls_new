<?php

namespace app\models\hdr1;

use Yii;

/**
 * This is the model class for table "swkbcategories".
 *
 * @property integer $kbcategoryid
 * @property integer $parentkbcategoryid
 * @property integer $staffid
 * @property string $title
 * @property integer $dateline
 * @property integer $totalarticles
 * @property integer $categorytype
 * @property integer $displayorder
 * @property integer $articlesortorder
 * @property integer $allowcomments
 * @property integer $allowrating
 * @property integer $ispublished
 * @property integer $uservisibilitycustom
 * @property integer $staffvisibilitycustom
 * @property integer $isimporteddownloadcategory
 */
class Swkbcategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'swkbcategories';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('hdr1');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parentkbcategoryid', 'staffid', 'dateline', 'totalarticles', 'categorytype', 'displayorder', 'articlesortorder', 'allowcomments', 'allowrating', 'ispublished', 'uservisibilitycustom', 'staffvisibilitycustom', 'isimporteddownloadcategory'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kbcategoryid' => 'Kbcategoryid',
            'parentkbcategoryid' => 'Parentkbcategoryid',
            'staffid' => 'Staffid',
            'title' => 'Title',
            'dateline' => 'Dateline',
            'totalarticles' => 'Totalarticles',
            'categorytype' => 'Categorytype',
            'displayorder' => 'Displayorder',
            'articlesortorder' => 'Articlesortorder',
            'allowcomments' => 'Allowcomments',
            'allowrating' => 'Allowrating',
            'ispublished' => 'Ispublished',
            'uservisibilitycustom' => 'Uservisibilitycustom',
            'staffvisibilitycustom' => 'Staffvisibilitycustom',
            'isimporteddownloadcategory' => 'Isimporteddownloadcategory',
        ];
    }
}
