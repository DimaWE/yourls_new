<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "SURVEY_TEMPLATES".
 *
 * @property integer $sv_tm_id
 * @property integer $sv_tm_srv_id
 * @property string $sv_tm_code
 * @property string $sv_tm_thank_you
 * @property integer $sv_tm_probability
 * @property integer $sv_tm_sv_t_t_id
 */
class SurveyTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SURVEY_TEMPLATES';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sv_tm_srv_id', 'sv_tm_probability', 'sv_tm_sv_t_t_id'], 'integer'],
            [['sv_tm_code', 'sv_tm_thank_you'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sv_tm_id' => 'Sv Tm ID',
            'sv_tm_srv_id' => 'Sv Tm Srv ID',
            'sv_tm_code' => 'Sv Tm Code',
            'sv_tm_thank_you' => 'Sv Tm Thank You',
            'sv_tm_probability' => 'Sv Tm Probability',
            'sv_tm_sv_t_t_id' => 'Sv Tm Sv T T ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyModel()
    {
        return $this->hasOne(SurveyModel::className(), ['srv_id' => 'sv_tm_srv_id']);
    }
}
