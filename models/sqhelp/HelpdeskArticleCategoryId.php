<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "HELPDESK_ARTICLE_CATEGORY_IDS".
 *
 * @property integer $id
 * @property string $helpdesk
 * @property integer $department
 * @property integer $survey_publisher_id
 * @property integer $survey_project_id
 * @property integer $survey_language_id
 */
class HelpdeskArticleCategoryId extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'HELPDESK_ARTICLE_CATEGORY_IDS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department', 'survey_publisher_id', 'survey_project_id', 'survey_language_id'], 'integer'],
            [['helpdesk'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'helpdesk' => 'Helpdesk',
            'department' => 'Department',
            'survey_publisher_id' => 'Survey Publisher ID',
            'survey_project_id' => 'Survey Project ID',
            'survey_language_id' => 'Survey Language ID',
        ];
    }
}
