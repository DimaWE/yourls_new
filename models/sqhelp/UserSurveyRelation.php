<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "USER_SURVEY_RELATIONS".
 *
 * @property integer $usr_srv_id
 * @property integer $usr_usr_id
 */
class UserSurveyRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'USER_SURVEY_RELATIONS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usr_srv_id', 'usr_usr_id'], 'required'],
            [['usr_srv_id', 'usr_usr_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usr_srv_id' => 'Usr Srv ID',
            'usr_usr_id' => 'Usr Usr ID',
        ];
    }
}
