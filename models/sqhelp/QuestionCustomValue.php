<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "QUESTION_CUSTOM_VALUES".
 *
 * @property integer $ct_vl_id
 * @property string $ct_vl_code
 * @property string $ct_vl_value
 * @property integer $ct_vl_srv_id
 * @property integer $ct_vl_qst_id
 * @property integer $ct_vl_lnk_id
 */
class QuestionCustomValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'QUESTION_CUSTOM_VALUES';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ct_vl_srv_id', 'ct_vl_qst_id', 'ct_vl_lnk_id'], 'integer'],
            [['ct_vl_code', 'ct_vl_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ct_vl_id' => 'Ct Vl ID',
            'ct_vl_code' => 'Ct Vl Code',
            'ct_vl_value' => 'Ct Vl Value',
            'ct_vl_srv_id' => 'Ct Vl Srv ID',
            'ct_vl_qst_id' => 'Ct Vl Qst ID',
            'ct_vl_lnk_id' => 'Ct Vl Lnk ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionSurveyRelations()
    {
        return $this->hasMany(QuestionSurveyRelation::className(), ['qsr_srv_id' => 'ct_vl_srv_id']);
    }
}
