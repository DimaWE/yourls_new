<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "PROJECT_TRANSLATIONS".
 *
 * @property integer $pj_tr_pjt_id
 * @property integer $pj_tr_lng_id
 * @property string $pj_tr_name
 */
class ProjectTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PROJECT_TRANSLATIONS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pj_tr_pjt_id', 'pj_tr_lng_id'], 'required'],
            [['pj_tr_pjt_id', 'pj_tr_lng_id'], 'integer'],
            [['pj_tr_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pj_tr_pjt_id' => 'Pj Tr Pjt ID',
            'pj_tr_lng_id' => 'Pj Tr Lng ID',
            'pj_tr_name' => 'Pj Tr Name',
        ];
    }
}
