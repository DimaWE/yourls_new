<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "SURVEY_TYPES".
 *
 * @property integer $sv_tp_id
 * @property string $sv_tp_code
 * @property string $sv_tp_name
 */
class SurveyType extends \yii\db\ActiveRecord
{
    public static $surveyTypes =[
        1 => 'Support',
        2 => 'Event',
        3 => 'Article',
        4 => 'Activity',
        5 => 'Game',
    ];

    public static $surveyIds = [
        'support'  => 1,
        'event'    => 2,
        'article'  => 3,
        'activity' => 4,
        'game'     => 5,
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SURVEY_TYPES';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sv_tp_code', 'sv_tp_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sv_tp_id' => 'Sv Tp ID',
            'sv_tp_code' => 'Sv Tp Code',
            'sv_tp_name' => 'Sv Tp Name',
        ];
    }
}
