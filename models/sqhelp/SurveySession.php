<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "SURVEY_SESSIONS".
 *
 * @property string $us_vt_session_id
 * @property integer $us_vt_lnk_id
 * @property integer $us_vt_srv_id
 * @property string $us_vt_form_code
 * @property integer $us_vt_dateline_start
 * @property integer $us_vt_dateline_end
 * @property string $us_vt_referer
 * @property string $us_vt_user_agent
 * @property string $us_vt_ip
 * @property integer $us_vt_tmp_id
 */
class SurveySession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SURVEY_SESSIONS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['us_vt_lnk_id', 'us_vt_srv_id', 'us_vt_dateline_start', 'us_vt_dateline_end', 'us_vt_tmp_id'], 'integer'],
            [['us_vt_referer', 'us_vt_user_agent'], 'string'],
            [['us_vt_session_id', 'us_vt_form_code', 'us_vt_ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'us_vt_session_id' => 'Us Vt Session ID',
            'us_vt_lnk_id' => 'Us Vt Lnk ID',
            'us_vt_srv_id' => 'Us Vt Srv ID',
            'us_vt_form_code' => 'Us Vt Form Code',
            'us_vt_dateline_start' => 'Us Vt Dateline Start',
            'us_vt_dateline_end' => 'Us Vt Dateline End',
            'us_vt_referer' => 'Us Vt Referer',
            'us_vt_user_agent' => 'Us Vt User Agent',
            'us_vt_ip' => 'Us Vt Ip',
            'us_vt_tmp_id' => 'Us Vt Tmp ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(Link::className(), ['lnk_id' => 'us_vt_lnk_id']);
    }
}
