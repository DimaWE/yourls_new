<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "QUESTION_SURVEY_RELATIONS".
 *
 * @property integer $qsr_srv_id
 * @property integer $qsr_qst_id
 */
class QuestionSurveyRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'QUESTION_SURVEY_RELATIONS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qsr_srv_id', 'qsr_qst_id'], 'required'],
            [['qsr_srv_id', 'qsr_qst_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'qsr_srv_id' => 'Qsr Srv ID',
            'qsr_qst_id' => 'Qsr Qst ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionCustomValues()
    {
        return $this->hasMany(QuestionCustomValue::className(), ['ct_vl_srv_id' => 'qsr_srv_id']);
    }
}
