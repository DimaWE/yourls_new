<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "LINKS".
 *
 * @property integer $lnk_id
 * @property integer $lnk_srv_id
 * @property string $lnk_code
 * @property integer $lnk_usr_id
 * @property string $lnk_creator
 * @property string $lnk_comment
 * @property integer $lnk_is_active
 * @property string $lnk_session_id
 * @property integer $lnk_dateline
 * @property integer $lnk_lnk_tp_id
 */
class Link extends \yii\db\ActiveRecord
{

    public static $surLinkText = [
        1 => [
            1  => 'Оценить ответ: {link}',
            2  => 'Rate this answer: {link}',
            3  => 'Bewerten Sie die Antwort: {link}',
            4  => 'Évaluer la réponse: {link}',
            5  => 'Calificar esta respuesta: {link}',
            6  => 'Votare questa risposta: {link}',
            7  => '応答を評価します: {link}',
            8  => '답을 평가해주세요: {link}',
            9  => 'Evaluar esta resposta: {link}',
            10 => '{link} bu sayfa üzerinde cevaba puan verin.',
            11 => '評價這個答案: {link}',
            12 => '评价这个答案: {link}'
        ],
        2 => [
            1  => 'Оценить конкурс: {link}',
            2  => 'Rate this event: {link}',
            3  => 'Bewerten Sie das Event: {link}',
            4  => 'Évaluer l\'événement : {link}',
            5  => 'Calificar este evento: {link}',
            6  => 'Votare questo evento: {link}',
            7  => 'イベントを評価します: {link}',
            8  => '이벤트를 평가해주세요: {link}',
            9  => 'Evaluar este evento: {link}',
            10 => '{link} bu sayfa üzerinde etkinliğe puan verin.',
            11 => '評價這個活動: {link}',
            12 => '评价这个事件: {link}'
        ],
        3 => [
            1  => 'Оценить статью: {link}',
            2  => 'Rate this article: {link}',
            3  => 'Bewerten Sie den Artikel: {link}',
            4  => 'Évaluer l\'article : {link}',
            5  => 'Calificar este artículo: {link}',
            6  => 'Votare questo articolo: {link}',
            7  => '記事を評価します: {link}',
            8  => '이 기사를 평가해주세요: {link}',
            9  => 'Evaluar este artigo: {link}',
            10 => '{link}  bu sayfa üzerinde makaleye puan verin.',
            11 => '本文評分: {link}',
            12 => '本文評分: {link}',
        ],
        4 => [
            1  => 'Оценить мероприятие: {link}',
            2  => 'Rate this activity: {link}',
            3  => 'Bewerten Sie die Aktivität: {link}',
            4  => 'Évaluer cette activité : {link}',
            5  => 'Calificar esta actividad: {link}',
            6  => 'Votare questa attività: {link}',
            7  => 'アクティビティを評価します: {link}',
            8  => '이벤트를 평가해주세요: {link}',
            9  => 'Evaluar esta actividade: {link}',
            10 => '{link} bu sayfa üzerinde etkinliğe puan verin.',
            11 => '評價這個活動：{link}',
            12 => '评价这个活动：{link}'
        ],
        5 => [
            1  => 'Оценить игру: {link}',
            2  => 'Rate this game: {link}',
            3  => 'Rate this game: {link}',
            4  => 'Rate this game: {link}',
            5  => 'Rate this game: {link}',
            6  => 'Rate this game: {link}',
            7  => 'Rate this game: {link}',
            8  => 'Rate this game: {link}',
            9  => 'Rate this game: {link}',
            10 => 'Rate this game: {link}',
            11 => 'Rate this game: {link}',
            12 => 'Rate this game: {link}'
        ]
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LINKS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lnk_srv_id', 'lnk_usr_id', 'lnk_is_active', 'lnk_dateline', 'lnk_lnk_tp_id'], 'integer'],
            [['lnk_comment'], 'string'],
            [['lnk_code', 'lnk_creator', 'lnk_session_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lnk_id' => 'Lnk ID',
            'lnk_srv_id' => 'Lnk Srv ID',
            'lnk_code' => 'Lnk Code',
            'lnk_usr_id' => 'Lnk Usr ID',
            'lnk_creator' => 'Lnk Creator',
            'lnk_comment' => 'Lnk Comment',
            'lnk_is_active' => 'Lnk Is Active',
            'lnk_session_id' => 'Lnk Session ID',
            'lnk_dateline' => 'Lnk Dateline',
            'lnk_lnk_tp_id' => 'Lnk Lnk Tp ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyModel()
    {
        return $this->hasOne(SurveyModel::className(), ['srv_id' => 'lnk_srv_id']);
    }

    public function imgSrc($img) {
	    return '/images/'.$this->surveyModel->randSurveyTemplate->sv_tm_code.'/'.$img;
    }

    public static function getCode() {
        $ok = false;
        do {
            $linkCode = substr(bin2hex(openssl_random_pseudo_bytes(3)), 0, 5);

            if (!preg_match('/^((?=.*[a-z])(?=.*[A-Z])[a-z0-9A-Z]{5})$/i', $linkCode)) {
                continue;
            }

            $link = self::find()
                ->select('lnk_id')
                ->where(['lnk_code' => $linkCode])
                ->one();

            if (empty($link)) {
                return $linkCode;
            }

        } while (!$ok);        
    }
}
