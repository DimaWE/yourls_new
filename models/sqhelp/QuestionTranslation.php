<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "QUESTION_TRANSLATIONS".
 *
 * @property integer $qt_tr_qst_id
 * @property integer $qt_tr_lng_id
 * @property string $qt_tr_text
 */
class QuestionTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'QUESTION_TRANSLATIONS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qt_tr_qst_id', 'qt_tr_lng_id'], 'required'],
            [['qt_tr_qst_id', 'qt_tr_lng_id'], 'integer'],
            [['qt_tr_text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'qt_tr_qst_id' => 'Qt Tr Qst ID',
            'qt_tr_lng_id' => 'Qt Tr Lng ID',
            'qt_tr_text' => 'Qt Tr Text',
        ];
    }
}
