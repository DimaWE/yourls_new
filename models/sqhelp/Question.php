<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "QUESTIONS".
 *
 * @property integer $qst_id
 * @property string $qst_code
 * @property string $qst_text
 * @property integer $qst_qt_tp_id
 * @property integer $qst_srv_tp_id
 * @property integer $qst_qabt_id
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'QUESTIONS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qst_text'], 'string'],
            [['qst_qt_tp_id'], 'required'],
            [['qst_qt_tp_id', 'qst_srv_tp_id', 'qst_qabt_id'], 'integer'],
            [['qst_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'qst_id' => 'Qst ID',
            'qst_code' => 'Qst Code',
            'qst_text' => 'Qst Text',
            'qst_qt_tp_id' => 'Qst Qt Tp ID',
            'qst_srv_tp_id' => 'Qst Srv Tp ID',
            'qst_qabt_id' => 'Qst Qabt ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionSurveyRelations()
    {
        return $this->hasMany(QuestionSurveyRelation::className(), ['qsr_qst_id' => 'qst_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionTranslations()
    {
        return $this->hasMany(QuestionTranslation::className(), ['qt_tr_qst_id' => 'qst_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionTranslation()
    {
        return $this->hasOne(QuestionTranslation::className(), ['qt_tr_qst_id' => 'qst_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerPresets()
    {
        return $this->hasMany(AnswerPreset::className(), ['ans_qst_id' => 'qst_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionCustomValues()
    {
        return $this->hasMany(QuestionCustomValue::className(), ['ct_vl_qst_id' => 'qst_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionCustomValue()
    {
        return $this->hasOne(QuestionCustomValue::className(), ['ct_vl_qst_id' => 'qst_id']);
    }


    public function getText() 
    {
        $qText = $this->questionTranslation->qt_tr_text;

        if ($this->questionCustomValue && !empty($this->questionCustomValue->ct_vl_value)) {
            $qText = str_replace('<' . $this->questionCustomValue->ct_vl_code . '>', $this->questionCustomValue->ct_vl_value, $qText);
        } 

        return $qText;
    }
}
