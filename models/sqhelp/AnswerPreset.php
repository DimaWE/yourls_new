<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "ANSWER_PRESETS".
 *
 * @property integer $ans_id
 * @property integer $ans_qst_id
 * @property string $ans_code
 * @property string $ans_text
 * @property string $ans_options
 * @property integer $ans_sv_tp_id
 * @property integer $ans_qabt_id
 */
class AnswerPreset extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ANSWER_PRESETS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ans_qst_id', 'ans_sv_tp_id', 'ans_qabt_id'], 'integer'],
            [['ans_text'], 'string'],
            [['ans_code', 'ans_options'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ans_id' => 'Ans ID',
            'ans_qst_id' => 'Ans Qst ID',
            'ans_code' => 'Ans Code',
            'ans_text' => 'Ans Text',
            'ans_options' => 'Ans Options',
            'ans_sv_tp_id' => 'Ans Sv Tp ID',
            'ans_qabt_id' => 'Ans Qabt ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerPresetTranslations()
    {
        return $this->hasMany(AnswerPresetTranslation::className(), ['an_tr_ans_id' => 'ans_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerPresetTranslation()
    {
        return $this->hasOne(AnswerPresetTranslation::className(), ['an_tr_ans_id' => 'ans_id']);
    }


    public function getText() 
    {
        return $this->answerPresetTranslation->an_tr_text;
    }

}
