<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "SURVEY_ANSWERS".
 *
 * @property string $us_rs_session_id
 * @property integer $us_rs_an_tp_id
 * @property integer $us_rs_qst_id
 * @property string $us_rs_value
 * @property integer $us_rs_dateline
 * @property integer $us_rs_srv_id
 * @property string $us_rs_form_code
 * @property integer $us_rs_lnk_id
 * @property integer $us_rs_tmp_id
 */
class SurveyAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SURVEY_ANSWERS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['us_rs_an_tp_id', 'us_rs_qst_id', 'us_rs_dateline', 'us_rs_srv_id', 'us_rs_lnk_id', 'us_rs_tmp_id'], 'integer'],
            [['us_rs_value'], 'string'],
            [['us_rs_session_id', 'us_rs_form_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'us_rs_session_id' => 'Us Rs Session ID',
            'us_rs_an_tp_id' => 'Us Rs An Tp ID',
            'us_rs_qst_id' => 'Us Rs Qst ID',
            'us_rs_value' => 'Us Rs Value',
            'us_rs_dateline' => 'Us Rs Dateline',
            'us_rs_srv_id' => 'Us Rs Srv ID',
            'us_rs_form_code' => 'Us Rs Form Code',
            'us_rs_lnk_id' => 'Us Rs Lnk ID',
            'us_rs_tmp_id' => 'Us Rs Tmp ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerPreset()
    {
        return $this->hasOne(AnswerPreset::className(), ['ans_id' => 'us_rs_value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['qst_id' => 'us_rs_qst_id']);
    }
}
