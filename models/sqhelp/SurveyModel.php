<?php

namespace app\models\sqhelp;

use Yii;
use app\models\cscommon\{Project, Language, Publisher, Channel};

/**
 * This is the model class for table "SURVEY_MODELS".
 *
 * @property integer $srv_id
 * @property integer $srv_sv_tp_id
 * @property integer $srv_pjt_id
 * @property integer $srv_pbs_id
 * @property integer $srv_cnl_id
 * @property integer $srv_lng_id
 * @property integer $srv_is_active
 */
class SurveyModel extends \yii\db\ActiveRecord
{
    private $_randSurveyTemplate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SURVEY_MODELS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['srv_sv_tp_id', 'srv_pjt_id', 'srv_pbs_id', 'srv_cnl_id', 'srv_lng_id', 'srv_is_active'], 'integer'],
            [['srv_sv_tp_id', 'srv_pjt_id', 'srv_pbs_id', 'srv_cnl_id', 'srv_lng_id'], 'unique', 'targetAttribute' => ['srv_sv_tp_id', 'srv_pjt_id', 'srv_pbs_id', 'srv_cnl_id', 'srv_lng_id'], 'message' => 'The combination of Srv Sv Tp ID, Srv Pjt ID, Srv Pbs ID, Srv Cnl ID and Srv Lng ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'srv_id' => 'Srv ID',
            'srv_sv_tp_id' => 'Srv Sv Tp ID',
            'srv_pjt_id' => 'Srv Pjt ID',
            'srv_pbs_id' => 'Srv Pbs ID',
            'srv_cnl_id' => 'Srv Cnl ID',
            'srv_lng_id' => 'Srv Lng ID',
            'srv_is_active' => 'Srv Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['pjt_id' => 'srv_pjt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['pbs_id' => 'srv_pbs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channel::className(), ['cnl_id' => 'srv_cnl_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['lng_id' => 'srv_lng_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyType()
    {
        return $this->hasOne(SurveyType::className(), ['sv_tp_id' => 'srv_sv_tp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSurveyRelations()
    {
        return $this->hasMany(UserSurveyRelation::className(), ['usr_srv_id' => 'srv_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyTemplates()
    {
        return $this->hasMany(SurveyTemplate::className(), ['sv_tm_srv_id' => 'srv_id']);
    }

    public function getRandSurveyTemplate()
    {   
        if(!$this->_randSurveyTemplate) {
            if($this->surveyTemplates) {
                $id = array_rand($this->surveyTemplates);
                $this->_randSurveyTemplate = $this->surveyTemplates[$id];
            }
        }
        return $this->_randSurveyTemplate;
    }
}
