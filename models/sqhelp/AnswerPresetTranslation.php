<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "ANSWER_PRESET_TRANSLATIONS".
 *
 * @property integer $an_tr_ans_id
 * @property integer $an_tr_lng_id
 * @property string $an_tr_text
 */
class AnswerPresetTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ANSWER_PRESET_TRANSLATIONS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['an_tr_ans_id', 'an_tr_lng_id'], 'required'],
            [['an_tr_ans_id', 'an_tr_lng_id'], 'integer'],
            [['an_tr_text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'an_tr_ans_id' => 'An Tr Ans ID',
            'an_tr_lng_id' => 'An Tr Lng ID',
            'an_tr_text' => 'An Tr Text',
        ];
    }
}
