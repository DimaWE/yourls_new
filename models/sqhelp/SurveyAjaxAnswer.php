<?php

namespace app\models\sqhelp;

use Yii;

/**
 * This is the model class for table "SURVEY_AJAX_ANSWERS".
 *
 * @property string $us_a_r_id
 * @property integer $us_a_r_an_tp_id
 * @property integer $us_a_r_qst_id
 * @property string $us_a_r_value
 * @property string $us_a_r_action
 * @property integer $us_a_r_dateline
 * @property integer $us_a_r_srv_id
 * @property string $us_a_r_form_code
 * @property string $us_a_r_session_id
 * @property integer $us_a_r_lnk_id
 * @property integer $us_a_r_tmp_id
 */
class SurveyAjaxAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SURVEY_AJAX_ANSWERS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sqhelp');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['us_a_r_an_tp_id', 'us_a_r_qst_id', 'us_a_r_dateline', 'us_a_r_srv_id', 'us_a_r_lnk_id', 'us_a_r_tmp_id'], 'integer'],
            [['us_a_r_value'], 'string'],
            [['us_a_r_action', 'us_a_r_form_code', 'us_a_r_session_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'us_a_r_id' => 'Us A R ID',
            'us_a_r_an_tp_id' => 'Us A R An Tp ID',
            'us_a_r_qst_id' => 'Us A R Qst ID',
            'us_a_r_value' => 'Us A R Value',
            'us_a_r_action' => 'Us A R Action',
            'us_a_r_dateline' => 'Us A R Dateline',
            'us_a_r_srv_id' => 'Us A R Srv ID',
            'us_a_r_form_code' => 'Us A R Form Code',
            'us_a_r_session_id' => 'Us A R Session ID',
            'us_a_r_lnk_id' => 'Us A R Lnk ID',
            'us_a_r_tmp_id' => 'Us A R Tmp ID',
        ];
    }
}
