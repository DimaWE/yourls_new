<?php

if (YII_ENV == 'dev') {
    return require(__DIR__ . '/db_dev.php');
} else {
    return require(__DIR__ . '/db_prod.php');
}