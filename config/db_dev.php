<?php
return [
    'cscommon' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=cscommon',
        'username' => 'root',
        'password' => '1',
        'charset' => 'utf8',
    ],
    'csparser' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=csparser',
        'username' => 'root',
        'password' => '1',
        'charset' => 'utf8mb4',
    ],
    'sqhelp' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=s-q.help',
        'username' => 'root',
        'password' => '1',
        'charset' => 'utf8',
    ],
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=s-q.help',
        'username' => 'root',
        'password' => '1',
        'charset' => 'utf8',
    ],
    'hdr1' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=helpdesk_ru1',
        'username' => 'root',
        'password' => '1',
        'charset' => 'utf8',
    ],
];