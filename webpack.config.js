var ExtractTextPlugin = require("extract-text-webpack-plugin");

var extractSass = new ExtractTextPlugin({
    filename: "web/css/style_[name].css"
});

module.exports = {
    entry: {
       bundle: './web/main.js',
       aa_1: './resource/stylesheets/aa_1/scss/style.scss',
       aa_2: './resource/stylesheets/aa_2/scss/style.scss',
       default: './resource/stylesheets/default/scss/style.scss',
       dw_1: './resource/stylesheets/dw_1/scss/style.scss',
       dw_2: './resource/stylesheets/dw_2/scss/style.scss',
       dz3d_1: './resource/stylesheets/dz3d_1/scss/style.scss',
       dz3d_2: './resource/stylesheets/dz3d_2/scss/style.scss',
       dznz_1: './resource/stylesheets/dznz_1/scss/style.scss',
       dznz_2: './resource/stylesheets/dznz_2/scss/style.scss',
       iaw_1: './resource/stylesheets/iaw_1/sass/style.scss',
       iaw_2: './resource/stylesheets/iaw_2/sass/style.scss',
       iaw_3: './resource/stylesheets/iaw_3/sass/style.scss',
       me_1: './resource/stylesheets/me_1/scss/style.scss',
       me_2: './resource/stylesheets/me_2/scss/style.scss',
       mk_1: './resource/stylesheets/mk_1/scss/style.scss',
       mk_2: './resource/stylesheets/mk_2/scss/style.scss',
       mk2_1: './resource/stylesheets/mk2_1/scss/style.scss',
       mk2_2: './resource/stylesheets/mk2_2/scss/style.scss',
       nz_1: './resource/stylesheets/nz_1/scss/style.scss',
       nz_2: './resource/stylesheets/nz_2/scss/style.scss',
       pj_1: './resource/stylesheets/pj_1/scss/style.scss',
       pj_2: './resource/stylesheets/pj_2/scss/style.scss',
       tf_1: './resource/stylesheets/tf_1/scss/style.scss',
       tf_2: './resource/stylesheets/tf_2/scss/style.scss',
       tk_1: './resource/stylesheets/tk_1/scss/style.scss',
       tk_2: './resource/stylesheets/tk_2/scss/style.scss'
       //wwnf_1: './resource/stylesheets/wwnf_1/scss/style.scss',
       //wwnf_2: './resource/stylesheets/wwnf_2/scss/style.scss'
    },
    output: {
        filename: 'web/js/[name].js'
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: extractSass.extract({
                use: [{
                    loader: "css-loader"
                }, {
                    loader: "autoprefixer-loader"
                }, {
                    loader: "sass-loader"
                }],
                fallback: "style-loader"
            })
        }]
    },
    plugins: [
        extractSass
    ], 
    watchOptions: { aggregateTimeoit: 300 }
};