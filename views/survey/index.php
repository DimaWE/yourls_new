<?php
use app\components\{Tools, Translate};

$this->title = 'SocialQuantum';
$this->registerCssFile('/css/style_'.$link->surveyModel->randSurveyTemplate->sv_tm_code.'.css');

echo $this->render('template/_type'.$link->surveyModel->randSurveyTemplate->sv_tm_sv_t_t_id,[        	
    'link'      => $link, 
    'questions' => $questions,
    'formCode'  => $formCode,
    'lngCode'   => $link->surveyModel->language->lng_code,
    'thankYou'  => $thankYou,
]);
?>

<script>
    $(window).bind("pageshow", function (event) {
        if (event.originalEvent.persisted) {
            window.location.reload()
        }
    });

    $('textarea').textareaAutoSize();

    $(document).on('change', 'input, textarea', function () {
        var name = $(this).attr('name');
        var value = $(this).val();

        var quest = $(this).attr('data-quest');
        var field = $(this).attr('data-field');
        var type = $(this).attr('data-type');  
        var answer = $(this).attr('data-answer');  

        if ($(this).attr('type') == 'checkbox') {
            value = $(this).prop('checked');
        }
        $.post(
            "<?=Tools::generateLink($link->lnk_code);?>",
            {
                name: name,
                value: value,
                quest: quest,
                field: field,
                type: type,
                answer: answer,
                form_code: $("input[name='form_code']").val(),
                sq_survey: $("input[name='sq_survey']").val(),
                sq_tp: $("input[name='sq_tp']").val()
            }
        );
    });

    $('form').on('submit', function () {
        $('input[type="submit"]').prop('disabled', true);
    });

    $(document).on('change', 'input[name*="[score]"], textarea#comment', function () {
        $('input[type="submit"]').prop('disabled', false);
    });

    $(document).on('keyup', 'textarea#comment', function () {
        var a = false;
        $.each($('input[name*="[score]"]'), function () {
            if ($(this).val()) {
                a = true;
            }
        });
        var text = $(this).val();
        text = text.trim();

        if (!a) {
            $('input[type="submit"]').prop('disabled', (text == ''));
        }
    });

    $('li.other textarea').on('click, focus', function () {
        if (!$(this).prevAll('input[type="checkbox"]').prop('checked')) {
            $(this).prevAll('input[type="checkbox"]').prop('checked', true).trigger('change');
        }
    });

    $('li.other input[type=checkbox]').on('change', function () {
        $(this).nextAll('textarea').val('').trigger('keyup');
    });

    function logic(id, score) {
        $('[data-target="' + id + '"], [data-additionally="' + id + '"]').toggleClass('hidden', score == 5);
    }

    <?php
    if (empty($thankYou)) {
        foreach ($questions as $i => $quest) {
            if ($quest->qst_qt_tp_id == 1) {
                echo "
                $('[data-rating=\"" . (Tools::encodeQ($quest->qst_id)) . "\"]').raty({
                    starType: 'i',
                    hints: [";
                //'ужасно', 'плохо', 'нормально', 'хорошо', 'отлично'
                switch ($quest->qst_qabt_id) {
                    case 1:
                        $type = 'quest_nps';
                        break;
                    case 2:
                        $type = 'quest_ces';
                        break;
                    case 3:
                        $type = 'quest_csat';
                        break;
                }
                foreach (Translate::t($type, $lngCode) as $key => $value) {
                    echo '\'' . $value . '\',';
                }
                echo "],
                    target: '[data-target=\"" . (Tools::encodeQ($quest->qst_id)) . "\"]',
                    targetText: '" . (Translate::t('Select an answer', $lngCode)) . "',
                    targetFormat: '<span>{score}</span>',
                    scoreName: 'answers[" . (Tools::encodeQ($quest->qst_id)) . "][score]',
                    targetKeep: true,
                    click: function (score, evt) {
                        var id = $(this).attr('data-rating');
                        logic(id, score);
                        input = $(this).children('input');
                        input.attr('data-quest',".$quest->qst_id.");
                        input.attr('data-field','score');
                        input.attr('data-type','number');            
                        return true;
                    }
                });
            ";
            }
        }
    }
    ?>
</script>