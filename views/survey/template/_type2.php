<?php
use app\components\{Tools, Translate};
?>
<div class="wrap">
    <div class="logo <?=$link->surveyModel->project->pjt_type . '_' . $lngCode; ?>"></div>
        <div class="glass_wrapper">

            <div class="g_head">
                <div class="g_row">
                    <div class="lul">&nbsp;</div>
                    <div class="uc">&nbsp;</div>
                    <div class="rur">&nbsp;</div>
                </div>
            </div>

            <div class="g_body">
                <div class="g_row">
                    <div class="lml"></div>
                    <?php if (!empty($thankYou)) { ?>
                        <div class="form_wrapper">
                            <div class="quest_section left_dialog">
                                <img draggable="false" src="<?=$link->imgSrc("dialog_1.png");?>" class="speaker">

                                <div class="quest">
                                    <img src="<?=$link->imgSrc("dialog_left.png");?>" class="bubble left" draggable="false">

                                    <div class="body">
                                        <h1 class="write"><?=Translate::t('Thank you for completing the survey!', $lngCode);?></h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="form_wrapper">
                            <form action="<?=Tools::generateLink($link->lnk_code);?>" method="POST">
                                <?php
                                foreach ($questions as $i => $quest) {
                                    if ($quest->qst_qt_tp_id == 1) {
                                        ?>
                                        <div class="quest_section <?=(($i + 1) % 2 == 0) ? 'right' : 'left';?>_dialog">
                                            <?=(($i + 1) % 2 == 0) ? '' : '<img draggable="false" src="'.$link->imgSrc("dialog_" . ($i + 1) . ".png").'" class="speaker">';?>
                                            <div class="quest">
                                                <img
                                                    src="<?=$link->imgSrc("dialog_".((($i + 1) % 2 == 0) ? 'right' : 'left').".png")?>"
                                                    class="bubble <?=(($i + 1) % 2 == 0) ? 'right' : 'left';?>"
                                                    draggable="false">

                                                <div class="body">
                                                    <p><?=$quest->text;?> </p>

                                                    <div class="rating" data-rating="<?=Tools::encodeQ($quest->qst_id); ?>"></div>
                                                    <div class="target hidden" data-target="<?=Tools::encodeQ($quest->qst_id); ?>">
                                                        <span><?=Translate::t('Select an answer', $lngCode); ?>
                                                            <span></span></div>
                                                    <div class="additionally hidden"
                                                        data-additionally="<?=Tools::encodeQ($quest->qst_id); ?>">
                                                        <ul>
                                                            <?php
                                                            foreach ($quest->answerPresets as $key => $value) {
                                                                if ($value->ans_options == 'other') {
                                                                    echo '
                                                            <li class="other">
                                                                <span class="counter">0 / 750</span>
                                                                <textarea data-quest="'.$quest->qst_id.'" data-field="additionally" data-type="text" maxlength="750" name="answers[' . (Tools::encodeQ($quest->qst_id)) . '][additionally][' . (Tools::encodeA($value->ans_id)) . '][text]"
                                                                    placeholder="' . Translate::t('We welcome any additional comments!',
                                                                            $lngCode) . '"></textarea>
                                                            </li>
                                                            ';                                              
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <?=(($i + 1) % 2 == 0) ? '<img draggable="false" src="'.$link->imgSrc("dialog_" . ($i + 1) . ".png").'" class="speaker">' : ''; ?>
                                        </div>

                                        <?php
                                    } elseif ($quest->qst_qt_tp_id == 2) {
                                        ?>
                                        <div class="quest_section <?=(($i + 1) % 2 == 0) ? 'right' : 'left' ?>_dialog">
                                            <?=(($i + 1) % 2 == 0) ? '' : '<img draggable="false" src="'.$link->imgSrc("dialog_" . ($i + 1) . ".png").'" class="speaker">'; ?>
                                            <div class="quest textarea">
                                                <img
                                                    src="<?=$link->imgSrc("dialog_".((($i + 1) % 2 == 0) ? 'right' : 'left').".png")?>"
                                                    class="bubble <?=(($i + 1) % 2 == 0) ? 'right' : 'left' ?>"
                                                    draggable="false">

                                                <div class="header"><?=$quest->text; ?></div>
                                                <div class="body">
                                                    <span class="counter">0 / 750</span>
                                                    <textarea data-quest="<?=$quest->qst_id;?>" data-field="comment" data-type="text" name="answers[<?=Tools::encodeQ($quest->qst_id); ?>][comment]"
                                                            id="comment" maxlength="750" style="white-space: pre-wrap;"
                                                            placeholder="<?=Translate::t('We welcome any additional comments!',
                                                                $lngCode); ?>"></textarea>
                                                </div>
                                            </div>
                                            <?=(($i + 1) % 2 == 0) ? '<img draggable="false" src="'.$link->imgSrc("dialog_" . ($i + 1) . ".png").'" class="speaker">' : ''; ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="quest_section">
                                    <input class="submit-btn btn-off" type="submit" value="<?=Translate::t('Send', $lngCode); ?>" disabled>
                                </div>
                                    </div>


                                    <div class="rmr"></div>
                                </div>
                            </div>

                            <div class="g_footer">
                                <div class="g_row">
                                    <div class="ldl"></div>
                                    <div class="dc">&nbsp;</div>
                                    <div class="rdr"></div>
                                </div>
                        </div>
                    <?php } ?>
            </div>

<?php if (empty($thankYou)) { ?>
    <div class="footer">
        <input type="hidden" name="form_code" value="<?=$formCode; ?>">
        <input type="hidden" name="sq_survey" value="<?=Tools::encrypt(session_id()); ?>">
        <input type="hidden" name="sq_tp" value="<?=$link->surveyModel->randSurveyTemplate->sv_tm_id; ?>">
        </form>
    </div>
<?php } ?>