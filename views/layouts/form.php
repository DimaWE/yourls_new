<?php

/* @var $this \yii\web\View */
/* @var $content string */
use Yii; 
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FormAsset;

FormAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru" class="no-js">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/favicon.ico"/>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <legend>SQ Survey Link 
                <span class="pull-right">
                    <?php echo isset(Yii::$app->user->identity->usr_name_cyr) ? Yii::$app->user->identity->usr_name_cyr : 'user'; ?>
                    | <?= Html::a('Выйти', ['/site/logout']) ?>
                </span>
            </legend>
        </div>
    </div>
    <?= $content ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
