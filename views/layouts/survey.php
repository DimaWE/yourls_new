<?php

/* @var $this \yii\web\View */

use Yii; 
use yii\helpers\Html;
use app\assets\SurveyAsset;

SurveyAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src='js/template/jquery-1.11.1.min.js'></script>
    <script src='js/bundle.js'></script>
    <!--<script src='js/template/jquery-1.11.1.min.js'></script>
    <script src='js/template/jquery.raty.js'></script>
    <script src='js/template/jquery.textarea_autosize.js'></script>
    <script src='js/template/jquery.limit.js'></script>-->
</head>
<body style="/*height: 100%;margin: 0;background-attachment: fixed;*/">
<?php $this->beginBody() ?>
    <?= $content ?>
<?php $this->endBody() ?>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-70262539-1', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
<?php $this->endPage() ?>
