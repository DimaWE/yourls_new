<?php
use app\models\sqhelp\SurveyType;

/* @var $this yii\web\View */

$this->title = 'SQ Survey Link';
?>

<div class="row">
    <ul class="nav nav-tabs col-md-8 col-md-offset-2" role="tablist">
        <li role="presentation" class="active">
            <a href="#makeSurvey" aria-controls="home" role="tab" data-toggle="tab">Make Survey Link</a>
        </li>
        <?php if (Yii::$app->user->identity->isAdmin()) { ?>
            <li role="presentation">
                <a href="#checkSurvey" aria-controls="profile" role="tab" data-toggle="tab">Check Survey Link</a>
                </li>
        <?php } ?>


        <?php if (Yii::$app->user->identity->usr_grp_id == 1) { ?>
            <li role="presentation">
                <a href="#addSurvey" aria-controls="profile" role="tab" data-toggle="tab">Add Survey</a>
            </li>
        <?php } ?>
    </ul>

    <div class="tab-content col-md-12">
        <div role="tabpanel" class="tab-pane active" id="makeSurvey">
            <form id="make">
                <div class="row">
                    <div class="col-md-2 col-md-offset-2">
                        <label>Тип сурвея</label>
                        <select name="tos" class="form-control" id="tos">
                            <option value="0">--</option>
                            <?php
                            if (Yii::$app->user->identity->isAdmin()) {
                                foreach (SurveyType::$surveyTypes as $k => $v) { ?>
                                    <option value="<?=$k;?>" <?=(!empty($_SESSION['remoteChained']['tos']) && $k == $_SESSION['remoteChained']['tos']) ? 'selected' : '' ?>>
                                    	<?=$v;?>                                  	
                                    </option>
                                <?php }
                            } else {
                                if (!empty($userSurveyType)) {
                                    foreach ($userSurveyType as $v) { ?>
                                        <option value="<?=$v->srv_sv_tp_id;?>" <?=(!empty($_SESSION['remoteChained']['tos']) && $v->srv_sv_tp_id == $_SESSION['remoteChained']['tos']) ? 'selected' : '' ?>>
                                        	<?=$v->surveyType->sv_tp_code;?>                                        	
                                        </option>
                                    <?php }
                                }
                            }

                            ?>
                        </select>
                    </div>


                    <div class="col-md-6">
                        <label>Комментарий</label>
                        <input type="text" name="comment" placeholder="" class="form-control">
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-2 col-md-offset-2">
                        <label>Проект</label>
                        <select name="project" class="form-control" id="project" disabled>
                            <option value="--">--</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label>Канал</label>
                        <select name="channel" class="form-control" id="channel" disabled>
                            <option value="--">--</option>
                        </select>
                    </div>


                    <div class="col-md-2">
                        <label>Издатель</label>
                        <select name="publisher" class="form-control" id="publisher" disabled>
                            <option value="--">--</option>
                        </select>
                    </div>

                    <div class="col-md-2 ">
                        <label>Язык</label>
                        <select name="language" class="form-control" id="language" disabled>
                            <option value="--">--</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-2">
                        <label>&nbsp;</label>
                        <input type="text" name="" placeholder="" class="form-control" id="custom_field" disabled>
                    </div>
                    <div class="col-md-2">
                        <label class="hidden-xs hidden-sm">&nbsp;</label>
                        <button type="submit" class="btn form-control btn-warning" disabled>Получить ссылку</button>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-1 col-md-offset-2 col-xs-2 copy first">
                    Ссылка:
                </div>
                <div class="col-md-7 form-group col-xs-10 copy_input-group first">
                    <div class="input-group">
                        <input type="text" class="form-control" disabled>
                        <span class="input-group-addon" data-copy="result_url">
                            <i class="glyphicon glyphicon-copy" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1 col-md-offset-2 col-xs-2 copy">
                    Текст:
                </div>
                <div class="col-md-7 form-group col-xs-10 copy_input-group">
                    <div class="input-group">
                        <input type="text" class="form-control" disabled>
                        <span class="input-group-addon" data-copy="result_text_url">
                            <i class="glyphicon glyphicon-copy" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-1 col-md-offset-2 col-xs-2 copy">
                    Форум:
                </div>
                <div class="col-md-7 form-group col-xs-10 copy_input-group">
                    <div class="input-group">
                        <input type="text" class="form-control" disabled>
                        <span class="input-group-addon" data-copy="result_forum_url">
                            <i class="glyphicon glyphicon-copy" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1 col-md-offset-2 col-xs-2 copy">
                    HTML:
                </div>
                <div class="col-md-7 form-group col-xs-10 copy_input-group">
                    <div class="input-group">
                        <input type="text" class="form-control" disabled>
                        <span class="input-group-addon" data-copy="result_link_text_url">
                            <i class="glyphicon glyphicon-copy" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <span class="message">&nbsp;</span>
                </div>
            </div>
        </div>
        <?php if (Yii::$app->user->identity->isAdmin()) { ?>
            <div role="tabpanel" class="tab-pane" id="checkSurvey">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <label class="hidden-xs hidden-sm">&nbsp;</label>
                        <input type="text" name="" placeholder="" class="form-control" id="get_info_text">
                    </div>
                    <div class="col-md-4">
                        <label class="hidden-xs hidden-sm">&nbsp;</label>
                        <a class="btn form-control btn-default" id="get_info">Получить информацию о ссылке</a>
                    </div>
                </div>

                <div class="row" id="get_info_output">
                    <div class="col-md-8 col-md-offset-2">
                    </div>
                </div>

            </div>
        <?php } ?>
        <?php if (Yii::$app->user->identity->usr_grp_id == 1) { ?>
            <div role="tabpanel" class="tab-pane" id="addSurvey">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <form id="create">
                            <select name="tos" class="form-control">
                                <?php
                                foreach (SurveyType::$surveyTypes as $k => $v) { ?>
                                    <option value="<?php echo $k; ?>">
                                    	<?php echo '(' . $k . ') ' . $v; ?>
                                    </option>
                                <?php }
                                ?>
                            </select>
                            <select name="project" class="form-control">
                                <?php
                                foreach ($projects as $project) { ?>
                                    <option value="<?php echo $project->pjt_id; ?>">
                                       	<?php echo '(' . $project->pjt_id . ') ' . $project->pjt_name . '(' . $project->pjt_type . ')'; ?>                                       	
                                    </option>
                                <?php }
                                ?>
                            </select>
                            <select name="channel" class="form-control">
                                <?php
                                foreach ($channels as $channel) { ?>
                                    <option value="<?php echo $channel->cnl_id; ?>">
                                    	<?php echo '(' . $channel->cnl_id . ') ' . $channel->cnl_name; ?> 	
                                    </option>
                                <?php }
                                ?>
                            </select>
                            <select name="publisher" class="form-control">
                                <?php
                                foreach ($publishers as $publisher) { ?>
                                    <option value="<?php echo $publisher->pbs_id; ?>">
                                    	<?php echo '(' . $publisher->pbs_id . ') ' . $publisher->pbs_name; ?>
                                    </option>
                                <?php }
                                ?>
                            </select>
                            <select name="language" class="form-control">
                                <?php
                                foreach ($languages as $language) { ?>
                                    <option value="<?php echo $language->lng_id; ?>">
                                    	<?php echo '(' . $language->lng_id . ') ' . $language->lng_name; ?>		
                                    </option>
                                <?php }
                                ?>
                            </select>
                            <input type="submit" value="Добавить сурвей">
                        </form>
                        <div class="survey_add_message"></div>
                    </div>
                </div>
            </div>

        <?php } ?>

    </div>
</div>