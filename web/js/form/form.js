$(document).ready(function () {
    var tos = $('#tos'),
        tos_type = tos.val(),
        custom_field = $('#custom_field'),
        message = $('.message'),
        activity_value = '';

    $("#project").remoteChained({
        parents: "#tos",
        url: "/form/json",
        loading: "Загрузка"
    });


    $("#channel").remoteChained({
        parents: "#tos, #project",
        url: "/form/json",
        loading: "Загрузка"
    });

    $("#publisher").remoteChained({
        parents: "#tos, #project, #channel",
        url: "/form/json",
        loading: "Загрузка"
    });

    $("#language").remoteChained({
        parents: "#tos, #project, #channel, #publisher",
        url: "/form/json",
        loading: "Загрузка"
    });

    if (Modernizr.touchevents || Modernizr.pointerevents) {
        $('span[data-copy]').on('click', function() {
            var text = $(this).prevAll('input').val(),
                copyElement = $('<input>', {id: 'tmp', type: 'text', value: text});

            $('#make').append(copyElement);

            copyElement.select();
            document.execCommand('copy');
            copyElement.remove();

            message.text("");
            message.text("Скопировал: " + $(this).prevAll('input').val());
        });
    } else {
        $('span[data-copy]').zclip({
            path: '/js/ZeroClipboard.swf',
            beforeCopy: function () {
                $('.message').text("");
            },
            copy: function () {
                return $(this).prevAll('input').val();
            },
            afterCopy: function () {
                $('.message').text("Скопировал: " + $(this).prevAll('input').val());
            }
        });
    }

    $("#language").change(function () {
        var lan_val = $(this).val();

        if (lan_val > 0) {
            $.get('json', {language: lan_val});

            $("button[type=submit]").prop("disabled", false);

            if (tos_type == 1) {
                custom_field.prop('disabled', true).prop('name',
                   'custom[game]').val('').prevAll('label').text('Название игры');
                var project_val = $("#project").val();
                var params = {"project_id": project_val, "language_id": lan_val};
                var posting = $.post("/form/game-name", params);

                posting.done(function (data) {
                    var result = jQuery.parseJSON(data);
                    $("input[name='custom[game]']").val(result.project_name);
                });
            }
            else if (tos_type == 2) {
                custom_field.prop('disabled', false).prop('name',
                   'custom[event]').prevAll('label').text('Название конкурса (обязательно)');

                if (custom_field.val() != '') {
                    $("button[type=submit]").prop("disabled", false);
                }
                else {
                    $("button[type=submit]").prop("disabled", true);
                }

                custom_field.on('keyup', function () {
                    var text = $(this).val();
                    text = text.trim();
                    $('button[type="submit"]').prop('disabled', (text == ''));
                });
            }
            else if (tos_type == 3) {
                custom_field.prop('disabled', true).prop('name',
                   'custom').val('').prevAll('label').text('\u00A0');
            }
            else if (tos_type == 4) {
                custom_field.prop('disabled', false).prop('name',
                   'custom[activity]').prevAll('label').text('Название мероприятия (обязательно)');

                if (custom_field.val() != '') {
                    $("button[type=submit]").prop("disabled", false);
                }
                else {
                    $("button[type=submit]").prop("disabled", true);
                }

                custom_field.on('keyup', function () {
                    var text = $(this).val();
                    text = text.trim();
                    $('button[type="submit"]').prop('disabled', (text == ''));
                });
            }
        }
        else {
            if (tos_type == 2 || tos_type == 4) {
                custom_field.prop('disabled', true).prop('name',
                   'custom').prevAll('label').text('\u00A0');
                $("button[type=submit]").prop("disabled", true);
            }
            else {
                custom_field.prop('disabled', true).prop('name',
                   'custom').val('').prevAll('label').text('\u00A0');
                $("button[type=submit]").prop("disabled", true);
            }
        }
    });

    tos.change(function () {
        // Сохраняем значения инпута для Event/Activity
        if (tos_type == 2 || tos_type == 4) {
            activity_value = custom_field.val();
        }
        if (tos.val() == 2 || tos.val() == 4) {
            custom_field.val(activity_value);
        }
        tos_type = tos.val();
    });

    if (tos.val() != '') {
        tos.trigger('change');
    }

    $("select").change(function () {
        $('span[data-copy]').prevAll('input').val('').prop('disabled', true);
        message.text('');
    });

    $('.nav-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })

});

$("form#make").on("submit", function (event) {
    event.preventDefault();
    var $form = $(this),
        disabled = $form.find(':input:disabled').removeAttr('disabled'),
        params = $form.serialize(),
        message = $('.message');

    disabled.attr('disabled', 'disabled');

    message.text('');
    $('span[data-copy]').prevAll('input').prop('disabled', false);

    var posting = $.post("/form/sur-add", params);

    posting.done(function (data) {

        var result = jQuery.parseJSON(data);

        if (result.status == 'success') {
            $('span[data-copy="result_url"]').prevAll('input').val(result.url);
            $('span[data-copy="result_text_url"]').prevAll('input').val(result.url_text);
            $('span[data-copy="result_forum_url"]').prevAll('input').val(result.forum_url);
            $('span[data-copy="result_link_text_url"]').prevAll('input').val(result.url_link_text);
            message.text(result.message);
        }

    });
});


$("#get_info").click(function () {
    var outputContainer = $('#get_info_output div'),
        code = $("#get_info_text").val().match(/(\w{5})$/);

    if (code) {
        var param = code[1];
        $.get(
            'http://yourls-new.sq/api/get-info',
            {
                link: param,
                hash: 'so9ru1leet6BahHaizugh7Shuamu3u'
            }
        ).done(function (response) {
            var myJSONString = JSON.stringify(response, undefined, 4);
            var myEscapedJSONString = myJSONString.replace(/\\n|\\r/g, "");

            outputContainer.remove('pre');
            outputContainer.html('<pre>' + myEscapedJSONString + '</pre>');
        });
    }
    else {
        outputContainer.remove('pre');
        outputContainer.html('<pre>Input error</pre>');
    }
});


$("form#create").on("submit", function (event) {
    event.preventDefault();
    var $form = $(this);
    var params = $form.serialize();

    $('.survey_add_message').text('');

    var posting = $.post("/form/sur-create", params);

    posting.done(function (data) {

        var result = jQuery.parseJSON(data);

        if (result.status == 'success') {
            $('.survey_add_message').text(result.message);
        }

    });
});
