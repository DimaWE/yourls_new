$(document).ready(function () {

  $('textarea[maxlength]').keyup(function () {
    //get the limit from maxlength attribute
    var limit = parseInt($(this).attr('maxlength'));
    //get the current text inside the textarea
    var text = $(this).val();
    //count the number of characters in the text
    var chars = text.length;
    //check if there are more characters then allowed
    if (chars > limit) {
      //and if there are use substr to get the text before the limit
      var new_text = text.substr(0, limit);
      //and change the current text with the new text
      $(this).val(new_text);
    }
    $(this).prevAll('span.counter').text(chars + " / " + limit);
  });

});

var originalVal = $.fn.val;
$.fn.val = function (value) {
  if (typeof value == 'undefined') {
// Getter
    if ($(this).is("textarea")) {
      return originalVal.call(this)
        .replace(/\r\n/g, '\n') // reduce all \r\n to \n
        .replace(/\r/g, '\n') // reduce all \r to \n (we shouldn't really need this line. this is for paranoia!)
        .replace(/\n/g, '\r\n'); // expand all \n to \r\n
// this two-step approach allows us to not accidentally catch a perfect \r\n
// and turn it into a \r\r\n, which wouldn't help anything.
    }
    return originalVal.call(this);
  }
  else {
// Setter
    return originalVal.call(this, value);
  }
};