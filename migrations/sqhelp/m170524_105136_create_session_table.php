<?php

use yii\db\Migration;

/**
 * Handles the creation of table `session`.
 */
class m170524_105136_create_session_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('session', [
            'id' => $this->string(),
            'expire' => $this->integer(),
            'data' => 'blob',
            'PRIMARY KEY (id)'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('session');
    }
}
