<?php
namespace app\components;

class Translate
{
    static private $translate = [
        'Question' => [
            'ru'  => 'Вопрос',
            'en'  => 'Question',
            'de'  => 'Frage',
            'fr'  => 'Question',
            'es'  => 'Cuestión',
            'it'  => 'Domanda',
            'jp'  => '質問',
            'ko'  => '질문',
            'pt'  => 'Pergunta',
            'tr'  => 'Soru',
            'zht' => '問題',
            'zhs' => '问题'
        ],
        'Select an answer' => [
            'ru'  => 'Выберите ответ',
            'en'  => 'Choose an answer',
            'de'  => 'Wählen Sie eine Antwort',
            'fr'  => 'Choisissez une réponse ',
            'es'  => 'Elija una respuesta',
            'it'  => 'Sceglie una risposta',
            'jp'  => '解答を選んでください。',
            'ko'  => '답변을 선택하세요',
            'pt'  => 'Escolha uma resposta',
            'tr'  => 'Cevaplardan birini seçin',
            'zht' => '選擇的回答',
            'zhs' => '选择的回答'
        ],
        'We welcome any additional comments!' => [
            'ru'  => 'Будем рады любым дополнительным комментариям!',
            'en'  => 'We\'ll be glad to see any additional comments!',
            'de'  => 'Wir freuen uns auf zusätzliche Kommentare!',
            'fr'  => 'Nous serions heureux de voir des commentaires supplémentaires!',
            'es'  => 'Estaremos encantados de recibir cualquier comentario adicional!',
            'it'  => 'Saremo lieti di vedere eventuali commenti!',
            'jp'  => 'ご意見をお願いしております。',
            'ko'  => '추가적인 의견을 적어주시면 감사하겠습니다!',
            'pt'  => 'Estamos felizes em receber quaisquer comentários adicionais!',
            'tr'  => 'Biz sizin yorumlarınızı görmekten memnuniyet duyarız',
            'zht' => '我們歡迎任何其他意見',
            'zhs' => '我们欢迎任何的补充意见'
        ],
        'Send' => [
            'ru'  => 'Отправить',
            'en'  => 'Send',
            'de'  => 'Senden',
            'fr'  => 'Envoyer',
            'es'  => 'Enviar',
            'it'  => 'Inviare',
            'jp'  => '送信',
            'ko'  => '보내기',
            'pt'  => 'Enviar',
            'tr'  => 'gönder',
            'zht' => '發送',
            'zhs' => '发送'
        ],
        'Thank you for completing the survey!' => [
            'ru'  => 'Спасибо за прохождение опроса!',
            'en'  => 'Thank you for responding to our survey!',
            'de'  => 'Danke für die Beantwortung des Fragebogens!',
            'fr'  => 'Merci d\'avoir répondu à notre enquête!',
            'es'  => 'Gracias por responder a nuestra encuesta!',
            'it'  => 'Grazie per aver risposto al nostro sondaggio!',
            'jp'  => 'ご協力、誠にありがとうございました。',
            'ko'  => '저희 조사에 응답해 주셔서 감사합니다!',
            'pt'  => 'Obrigado por responder a nossa pesquisa!',
            'tr'  => 'Cevaplarınız için teşekkür ederiz ',
            'zht' => '感謝您填寫在調查!',
            'zhs' => '感谢您填写该调查!'
        ],
        'quest_nps'  => [
            'ru'  => [
                'Исключено',
                'Маловероятно',
                'Возможно',
                'Скорее всего',
                'Однозначно'
            ],
            'en'  => [
                'Extremely unlikely',
                'Unlikely',
                'Possibly',
                'Likely',
                'Extremely likely'
            ],
            'de'  => [
                'Äusserst unwahrscheinlich',
                'Unwahrscheinlich',
                'Möglich',
                'Wahrscheinlich',
                'Äusserst wahrscheinlich'
            ],
            'fr'  => [
                'Extrêmement improbable',
                'Improbable',
                'Peut-être',
                'Probable',
                'Extrêmement probable'
            ],
            'es'  => [
                'Muy poco probable',
                'Poco probable',
                'Quizá',
                'Probable',
                'Muy probable'
            ],
            'it'  => [
                'Molto improbabile',
                'Improbabile',
                'Possibile',
                'Probabile',
                'Molto probabile'
            ],
            'jp'  => [
                '絶対お勧めしない',
                'お勧めしない',
                'まあまあ',
                'お勧めできる',
                '非常にお勧めできる'
            ],
            'ko'  => [
                '절대 추천 안 함',
                '추천 안 함',
                '추천할 수도 있음',
                '추천함',
                '강력히 추천함'
            ],
                'pt'  => [
                'Extremamente improvável',
                'Improvável',
                'Possível',
                'Provável',
            'Extremamente provável'
                ],
            'tr'  => [
                'Çok düşük olasilik',
                'Düşük olasilik',
                'Muhtemel',
                'Büyük olasilik',
                'Çok yüksek olasilik'
            ],
            'zht' => [
                '極不可能',
                '不可能',
                '可能',
                '很可能',
                '極其可能'
            ],
                'zhs' => [
                '完全不可能',
                '不可能',
                '有可能',
                '可能',
                '非常可能'
                ],
        ],
        'quest_ces' => [
            'ru'  => [
                'Очень трудно',
                'Трудно',
                'Средне',
                'Легко',
                'Очень легко'
            ],
            'en'  => [
                'Next to impossible',
                'Difficult',
                'Neither',
                'Easy',
                'Very easy'
            ],
            'de'  => [
                'Nahezu unmöglich',
                'Schwierig',
                'Weder noch',
                'Einfach',
                'Sehr einfach'
            ],
            'fr'  => [
                'Presque impossible',
                'Difficile',
                'Ni l’un ni l’autre',
                'Facile',
                'Très facile'
            ],
            'es'  => [
                'Casi imposible',
                'Difícil',
                'Ninguna de las dos',
                'Fácil',
                'Muy fácil'
            ],
            'it'  => [
                'Quasi impossibile',
                'Difficile',
                'Né l’uno né l’altro',
                'Facile',
                'Molto facile'
            ],
            'jp'  => [
                'すごく難しかった',
                '難しかった',
                'ふつう',
                '簡単だった',
                'とても簡単だった'
            ],
            'ko'  => [
                '거의 불가능',
                '어려움',
                '보통',
                '쉬움',
                '매우 쉬움'
            ],
            'pt'  => [
                'Quase impossível',
                'Difícil',
                'Nenhum',
                'Fácil',
                'Muito fácil'
            ],
            'tr'  => [
                'Neredeyse imkansiz',
                'Zor',
                'Hiçbiri',
                'Kolay',
                'Çok kolay'
            ],
            'zht' => [
                '幾乎辦不到',
                '困難',
                '中等',
                '容易',
                '非常容易'
            ],
            'zhs' => [
                '几乎不可能',
                '困难',
                '不难不易',
                '容易',
                '非常容易'
            ],
        ],
        'quest_csat' => [
            'ru'  => [
                'Крайне недоволен',
                'Недоволен',
                'Затрудняюсь ответить',
                'Доволен',
                'Очень доволен'
            ],
            'en'  => [
                'Totally dissatisfied',
                'Dissatisfied',
                'Neutral',
                'Satisfied',
                'Completely satisfied'
            ],
            'de'  => [
                'Völlig unzufrieden',
                'Unzufrieden',
                'Neutral',
                'Zufrieden',
                'Völlig zufrieden'
            ],
            'fr'  => [
                'Totalement insatisfait',
                'Insatisfait',
                'Neutre',
                'Satisfait',
                'Totalement satisfait'
            ],
            'es'  => [
                'Muy poco satisfecho',
                'Poco satisfecho',
                'Neutral',
                'Satisfecho',
                'Muy satisfecho'
            ],
            'it'  => [
                'Completamente insoddisfatto/a',
                'Insoddisfatto/a',
                'Indifferente',
                'Soddisfatto/a',
                'Completamente soddisfatto/a'
            ],
            'jp'  => [
                'まったく満足できなかった',
                '満足できなかった',
                'ふつう',
                '満足した',
                'とても満足した'
            ],
            'ko'  => [
                '매우 실망',
                '불만족',
                '보통',
                '만족',
                '매우 만족'
            ],
            'pt'  => [
                'Totalmente insatisfeito(a)',
                'Insatisfeito(a)',
                'Neutro',
                'Satisfeito(a)',
                'Totalmente satisfeito(a)'
            ],
            'tr'  => [
                'Hiç memnun değil',
                'Memnun değil',
                'Kararsiz',
                'Memnun',
                'Çok memnun'
            ],
            'zht' => [
                '完全不滿意',
                '不滿意',
                '中等',
                '滿意',
                '完全滿意'
            ],
            'zhs' => [
                '完全不满意',
                '不满意',
                '居中',
                '满意',
                '完全满意'
            ],
        ],

        'quest_game_feedback' => [
            'ru'  => [
                'Плохо',
                'Так себе',
                'Играть можно',
                'Очень хорошо',
                'Просто супер!'
            ],
            'en'  => [
                'Bad',
                'So-so',
                'Fairly good',
                'Very good',
                'Excellent'
            ],
            'de'  => [
                'Schlecht',
                'Soso lala',
                'Passt',
                'Sehr gut',
                'Ausgezeichnet'
            ],
            'fr'  => [
                'Mal',
                'Comme ci comme ça',
                'On peut jouer',
                'Très bien',
                'Excellent'
            ],
            'es'  => [
                'Malo',
                'Más o menos',
                'Bastante bueno',
                'Muy bueno',
                'Excelente'
            ],
            'it'  => [
                'Male',
                'Cosi cosi',
                'Si può giocare',
                'Molto bene',
                'Ottimo'
            ],
            'jp'  => [
                'Bad',
                'So-so',
                'Fairly good',
                'Very good',
                'Excellent'
            ],
            'ko'  => [
                'Bad',
                'So-so',
                'Fairly good',
                'Very good',
                'Excellent'
            ],
            'pt'  => [
                'Não é bom',
                'IAssim-assim',
                'Se pode jogar',
                'Muito bem',
                'Excelente'
            ],
            'tr'  => [
                'Bad',
                'So-so',
                'Fairly good',
                'Very good',
                'Excellent'
            ],
            'zht' => [
                'Bad',
                'So-so',
                'Fairly good',
                'Very good',
                'Excellent'
            ],
            'zhs' => [
                'Bad',
                'So-so',
                'Fairly good',
                'Very good',
                'Excellent'
            ],
        ]
    ];

    public static function t($txt, $lang)
    {
        return self::$translate[$txt][$lang] ?? $text;
    }
}
