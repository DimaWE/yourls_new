<?php
namespace app\components;

/**
 * This is Tools class
 */
class Tools
{
    const SSL_ENCRYPTION_METHOD = 'AES-256-CBC';
    const SSL_PASSWORD = '25c6c7ff35b9979b151f2136cd13b0ff';
    const SSL_INITILIZATION_VECTOR = 'oahahC3ooghau3ia';

    public static function generateLink($link_code)
    {
        return 'http://yourls-new.sq/' . $link_code;
    }

    /**
     * Encode Q
     * 
     * @param string $qId
     * @return string
     */
    public static function encodeQ($qId)
    {
        return 'q' . $qId;
    }

    /**
     * Encode A
     * 
     * @param string $aId
     * @return string
     */
    public static function encodeA($aId)
    {
        return 'a' . $aId;
    }

    public static function decodeQ($qId)
    {
        return str_replace('q', '', $qId);
    }

    public static function decodeA($aId)
    {
        return str_replace('a', '', $aId);
    }

    public static function encrypt($data)
    {
        return openssl_encrypt($data, self::SSL_ENCRYPTION_METHOD, self::SSL_PASSWORD, 0, self::SSL_INITILIZATION_VECTOR);
    }
}
