import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserModule, By } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdInputModule, MdButtonModule, MdIconModule } from '@angular/material';

describe('AppComponent', () => {
  let de: DebugElement;
  let div: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
       AppComponent,
      ],
      imports: [
         HttpClientTestingModule, BrowserModule, BrowserAnimationsModule, HttpModule, FormsModule, MdInputModule, MdButtonModule, MdIconModule, 
      ],
    }).compileComponents();
  });

  // it('should create the app', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app).toBeTruthy();
  // }));

  // it(`should have as title 'app'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app');
  // }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    de  = fixture.debugElement.query(By.css('h2'));
  });

  it('should render title in a h1 tag', () => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      //de.nativeElement;
     expect(de.nativeElement.textContent).toContain('Wel!');
    });
    //expect(div.textContent).toContain('Welcome to Дмитрий Шевцов!');
    //fixture.detectChanges();
    //const compiled = fixture.debugElement.nativeElement;
    //expect(compiled.querySelector('h1').textContent).toContain('Welcome to Дмитрий Шевцов!');
  });
});
