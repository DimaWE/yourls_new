import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Clipboard } from 'ts-clipboard';
import $ from "jquery-ts";

class User {
  usr_id: number;
  usr_name_cyr: string;
  usr_name_lat: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'app';
  url: string = 'http://yourls-new.sq/api/user/get?userId=267';
  user: User;
  cp: string;
  constructor(private http: Http) {
    this.getUsers().then(user => {
      this.user = user;
      //console.log(this.user);
      //console.log(this.user.printText()); 
    });
  }

  getUsers(): Promise<User> {
    return this.http.get(this.url)
      .toPromise()
      .then(response => response.json() as User);
  }

  testCopy(): void {
    Clipboard.copy(this.cp);
    $('#test').toggle();
  }
}
