<?php

namespace app\controllers\api;

use Yii;
use yii\web\Controller;
use app\models\User;

class UserController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter' ] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $format = [
            'application/json' => \yii\web\Response::FORMAT_JSON
        ];

        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => $format,
        ];
        return $behaviors;
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        if (file_get_contents('php://input')) {
            $formData = json_decode(file_get_contents('php://input')) ?? [];
            foreach ($formData as $key => $value) {
                $_POST[$key] = $value;
            }
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return User::find()->all();
    }

    public function actionGet($userId = null)
    {
        if ($userId) {
            return User::find()->select('usr_id, usr_name_cyr, usr_name_lat')->where(['usr_id' => $userId])->one() ?? [];
        } else {
            return User::find()->select('usr_id, usr_name_cyr, usr_name_lat')->all();
        }
    }
}
