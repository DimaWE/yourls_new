<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\sqhelp\Link;
use app\models\sqhelp\Question;
use app\models\sqhelp\SurveySession;
use app\models\sqhelp\SurveyAnswer;
use app\models\sqhelp\SurveyAjaxAnswer;
use app\components\Tools;

class SurveyController extends Controller
{
    public $layout = 'survey';
    public $enableCsrfValidation = false;

    public function actionIndex($linkCode)
    {
        Yii::$app->session->open();
        $thankYou = false;
        $lnkIsActive = 1;
        $link = Link::find()
            ->joinWith(['surveyModel s' => function ($q) {
                $q->joinWith('language ln', true, 'INNER JOIN');
                $q->joinWith('project p', true, 'INNER JOIN');
                $q->joinWith('surveyTemplates st', true, 'INNER JOIN');
            }], true, 'INNER JOIN')
            ->where([
                'lnk_code' => $linkCode,
                'lnk_is_active' => $lnkIsActive,
            ])->one();

        if (!$link) {
            throw new \yii\web\HttpException(404, 'Survey not found');
        }

        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax) {
            if (!empty($post)) {
                $type = 1;
                $result = [
                    'qst_id' => $post['quest'],
                    'value'  => $post['answer'] ?? substr($post['value'], 0, 750),
                    'action' => $post['field'] == 'additionally' && $post['type'] == 'number' ? ($post['value'] == 'true' ? 'on' : 'off'): '',
                ];

                if ($post['field'] == 'score') {
                    $result['type'] = 1;
                } elseif ($post['field'] == 'additionally') {
                    $result['type'] = $post['type'] == 'number' ? 2 : 3;
                } elseif ($post['field'] == 'comment') {
                    $result['type'] = 4;
                }

                if (isset($post['form_code'])) {
                    $dateline = time();
                    $formCode = isset($_SESSION['form_code']) ? $_SESSION['form_code'] : $post['form_code'];
                    $templateId = isset($_SESSION['template_id']) ? $_SESSION['template_id'] : $post['sq_tp'];

                    $surveyAjaxAnswerModel = new SurveyAjaxAnswer();
                    $surveyAjaxAnswerData = [
                        'us_a_r_an_tp_id'   => $result['type'],
                        'us_a_r_qst_id'     => $result['qst_id'],
                        'us_a_r_value'      => $result['value'],
                        'us_a_r_action'     => $result['action'],
                        'us_a_r_dateline'   => $dateline,
                        'us_a_r_srv_id'     => $link->surveyModel->srv_id,
                        'us_a_r_form_code'  => $formCode,
                        'us_a_r_session_id' => Yii::$app->session->getId(),
                        'us_a_r_lnk_id'     => $link->lnk_id,
                        'us_a_r_tmp_id'     => $templateId,
                    ];

                    $surveyAjaxAnswerModel->attributes = $surveyAjaxAnswerData;
                    $surveyAjaxAnswerModel->save();
                }
            }
            return;
        }

        if (!empty($post)) {
            $results = [];

            if (isset($post['answers']) && !empty($post['answers'])) {
                foreach ($post['answers'] as $q_id => $q_value) {
                    $q_id = Tools::decodeQ($q_id);

                    if (isset($q_value['score']) && !empty($q_value['score'])) {
                        $results[] = [
                            'qst_id' => $q_id,
                            'type'   => '1',
                            'value'  => $q_value['score']
                        ];
                    }

                    if (isset($q_value['additionally']) && !empty($q_value['additionally'])) {
                        foreach ($q_value['additionally'] as $a_id => $a_value) {
                            $a_id = Tools::decodeA($a_id);
                            if (is_array($a_value)) {
                                if (isset($a_value['value'])) {
                                    $results[] = [
                                        'qst_id' => $q_id,
                                        'type'   => '2',
                                        'value'  => $a_id
                                    ];
                                }

                                if (isset($a_value['text']) && !empty($a_value['text'])) {
                                    $results[] = [
                                        'qst_id' => $q_id,
                                        'type'   => '3',
                                        'value'  => substr($a_value['text'], 0, 750),
                                    ];
                                }
                            } else {
                                $results[] = [
                                    'qst_id' => $q_id,
                                    'type'   => '2',
                                    'value'  => $a_id
                                ];
                            }
                        }
                    }

                    if (isset($q_value['comment']) && !empty($q_value['comment'])) {
                        $results[] = [
                            'qst_id' => $q_id,
                            'type'   => '4',
                            'value'  => $q_value['comment']
                        ];
                    }
                }
            }

            if (!empty($results) && isset($post['form_code'])) {
                $dateline = time();
                $formCode = isset($_SESSION['form_code']) ? $_SESSION['form_code'] : $post['form_code'];
                $templateId = isset($_SESSION['template_id']) ? $_SESSION['template_id'] : $post['sq_tp'];

                foreach ($results as $result) {
                    $surveyAnswerModel = new SurveyAnswer();
                    $surveyAnswerData = [
                        'us_rs_session_id' => Yii::$app->session->getId(),
                        'us_rs_an_tp_id'   => $result['type'],
                        'us_rs_qst_id'     => $result['qst_id'],
                        'us_rs_value'      => $result['value'],
                        'us_rs_dateline'   => $dateline,
                        'us_rs_srv_id'     => $link->surveyModel->srv_id,
                        'us_rs_form_code'  => $formCode,
                        'us_rs_lnk_id'     => $link->lnk_id,
                        'us_rs_tmp_id'     => $templateId,
                    ];
                    $surveyAnswerModel->attributes = $surveyAnswerData;
                    $surveyAnswerModel->save();
                }

                SurveySession::updateAll([
                        'us_vt_dateline_end' => $dateline
                    ], [
                        'us_vt_session_id' => Yii::$app->session->getId(),
                        'us_vt_form_code'  =>  $formCode,
                        'us_vt_srv_id'     => $link->surveyModel->srv_id,
                        'us_vt_lnk_id'     => $link->lnk_id,
                    ]);

                $_SESSION['write'] = true;
            }

            $thankYou = true;
        } else {
            $questions = Question::find()
                ->joinWith(['questionSurveyRelations qs' => function ($q) use ($link) {
                    $q->onCondition([
                        'qs.qsr_srv_id' => $link->surveyModel->srv_id
                    ]);
                }], false, 'JOIN')
                ->joinWith(['questionTranslation qt' => function ($q) use ($link) {
                    $q->onCondition([
                        'qt.qt_tr_lng_id' => $link->surveyModel->srv_lng_id,
                    ]);
                }], true, 'INNER JOIN')
                ->joinWith(['answerPresets a' => function ($q) use ($link) {
                    $q->joinWith(['answerPresetTranslation at' => function ($q) use ($link) {
                        $q->onCondition([
                            'at.an_tr_lng_id' => $link->surveyModel->srv_lng_id
                        ]);
                    }], true, 'INNER JOIN');
                }], true, 'INNER JOIN')
                ->joinWith(['questionCustomValue cv' => function ($q) use ($link) {
                    $q->joinWith('questionSurveyRelations', false, 'LEFT JOIN');
                    $q->onCondition([
                        'cv.ct_vl_lnk_id' => $link->lnk_id,
                    ]);
                }], true, 'LEFT JOIN')
                ->orderBy('length(qst_code), qst_code ASC, length(a.ans_code), a.ans_code ASC')
                ->all();

            if (empty($questions)) {
                throw new \yii\web\HttpException(404, 'Survey not found');
            } else {
                $userAgent = $_SERVER['HTTP_USER_AGENT'];
                $ip = $_SERVER['REMOTE_ADDR'];
                $datelineStart = time();
                $tId = $link->surveyModel->randSurveyTemplate->sv_tm_id ?? '';
                $formCode = bin2hex(openssl_random_pseudo_bytes(8));

                $surveySessionModel = new SurveySession();
                $surveySessionData = [
                    'us_vt_session_id'      => Yii::$app->session->getId(),
                    'us_vt_user_agent'      => $userAgent,
                    'us_vt_ip'              => $ip,
                    'us_vt_form_code'       => $formCode,
                    'us_vt_srv_id'          => $link->surveyModel->srv_id,
                    'us_vt_dateline_start'  => $datelineStart,
                    'us_vt_lnk_id'          => $link->lnk_id,
                    'us_vt_tmp_id'          => $tId,
                ];
                $surveySessionModel->attributes = $surveySessionData;
                $surveySessionModel->save();

                $_SESSION['form_code'] = $formCode;
                $_SESSION['template_id'] = $tId;
            }
        }
        
        return $this->render('index', [
            'link'      => $link,
            'questions' => $questions ?? null,
            'formCode'  => $formCode,
            'lngCode'   => $link->surveyModel->language->lng_code,
            'thankYou'  => $thankYou,
        ]);
    }
}
