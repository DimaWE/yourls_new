<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\sqhelp\{Link};

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'logout' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['form/index']);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['form/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['form/index']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionHdhelp() {
        //169 170 171 172
        //$art = \app\models\hdr1\Swkbarticles::find()->all();
        $db = Yii::$app->hdr1;
        $idsArts = $db->createCommand('SELECT DISTINCT(kbarticleid) FROM `swkbarticlelinks` WHERE linktypeid = 169 OR linktypeid = 170 OR linktypeid = 171 OR linktypeid = 172')->queryColumn();
        
        $arts = [];
        foreach($idsArts as $key => $art) {
            $artName = $db->createCommand('SELECT subject FROM swkbarticles WHERE kbarticleid = '.$art)->queryScalar();
            //$arts[$key] = ['title' => $artName, 'parent' => $this->getCategories($art)];

            echo "<b>-- ".$artName."</b><br>";
            foreach($this->getCategories($art) as $value) {
                echo "---- ".$value."<br>";
            }
            echo "<br><br>";
            
        }

    }

    public function getCategories($artId) {
        $db = Yii::$app->hdr1;
        $idsCat = $db->createCommand('SELECT linktypeid FROM `swkbarticlelinks` WHERE kbarticleid = '.$artId)->queryColumn();
        $result = [];
        foreach($idsCat as $catId) {
            $catInfo = $db->createCommand('SELECT title, parentkbcategoryid  FROM `swkbcategories` WHERE kbcategoryid = '.$catId)->queryOne();
            $str = $catInfo['title'];
            $pC = $catInfo['parentkbcategoryid'];
            while($pC != 0) {
                $pInfo = $db->createCommand('SELECT title, parentkbcategoryid  FROM `swkbcategories` WHERE kbcategoryid = '.$pC)->queryOne();
                $str  = $pInfo['title']." >> ".$str;
                $pC = $pInfo['parentkbcategoryid'];
            }
        if($str)
            $result[] = $str;
        }
        return $result;
    }
}
