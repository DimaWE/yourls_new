<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\cscommon\{Channel, Language, Project, Publisher};
use app\models\sqhelp\{SurveyModel, UserSurveyRelation, QuestionCustomValue, Link, ProjectTranslation, Question, QuestionSurbeyRelation, SurveyTemplate};
use app\components\{Tools};

class FormController extends Controller
{
	public $layout = 'form';

	public function actionIndex() 
	{
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }

        $userSurveyType = SurveyModel::find()
        	->select('srv_sv_tp_id')
        	->joinWith('userSurveyRelations AS c', false, 'INNER JOIN')
        	->joinWith('surveyType AS st', true, 'INNER JOIN')
        	->where([
        		'c.usr_usr_id' => Yii::$app->user->identity->usr_id,
        	])->all();

        $data = [
        	'channels'       => Channel::find()->all(),
        	'languages'      => Language::find()->all(),
        	'projects'       => Project::find()->all(),
        	'publishers'     => Publisher::find()->all(),
        	'userSurveyType' => $userSurveyType,
        ];
		return $this->render('index', $data);
	} 

	public function actionJson() 
	{
		if(Yii::$app->request->isAjax) {
		    $tos = Yii::$app->request->get('tos');
	        $project = Yii::$app->request->get('project');
	        $channel = Yii::$app->request->get('channel');
	        $publisher = Yii::$app->request->get('publisher');
	        $language = Yii::$app->request->get('language');

		    $userId = Yii::$app->user->identity->usr_id;
		    $surveyIsActive = 1;

		    if($language) {
		    	$_SESSION['remoteChained']['language'] = $language;
		    }

		    if($tos && $project && $channel && $publisher) {
				$languageQuery = Language::find()
	        		->select('DISTINCT(lng_id), lng_name')
	        		->where([
	        			's.srv_sv_tp_id'  => $tos,
	        			's.srv_is_active' => $surveyIsActive,
	        			'srv_pjt_id'      => $project,
	        			'srv_cnl_id'      => $channel,
	        			'srv_pbs_id'      => $publisher,
	        		]);

		        if (Yii::$app->user->identity->isAdmin()) {
		        	$languageQuery->joinWith('surveyModels AS s', false, 'INNER JOIN');
		        } else {
		        	$languageQuery->joinWith([
		        			'surveyModels AS s' => function($q) {
		        				$q->joinWith('userSurveyRelations AS cms', false, 'INNER JOIN');
		        			}
		        		], false, 'INNER JOIN')
		        		->andWhere([
		        			'cms.usr_usr_id'  => $userId,
		        		]);
		        }
		        $languageModels = $languageQuery->all();

		        $answer = ['0' => '--'];

		        if (!empty($languageModels)) {
		            foreach ($languageModels as $languageModel) {
		                $answer[$languageModel->lng_id] = $languageModel->lng_name;
		                if (!empty($_SESSION['remoteChained']['language']) && $_SESSION['remoteChained']['language'] == $languageModel->lng_id) {
		                    $languageSel = $languageModel->lng_id;
		                }
		            }

		            $answer['selected'] = $languageSel ?? (count($languageModels) == 1 ? $languageModels[0]->lng_id : "0");
		            $_SESSION['remoteChained']['language'] = $answer['selected'];
		        }

		        $_SESSION['remoteChained']['tos'] = $tos;
		        $_SESSION['remoteChained']['project'] = $project;
		        $_SESSION['remoteChained']['channel'] = $channel;
		        $_SESSION['remoteChained']['publisher'] = $publisher;

		        return json_encode($answer);
		    } elseif($tos && $project && $channel) {
		        $publisherQuery = Publisher::find()
	        		->select('DISTINCT(pbs_id), pbs_name')
	        		->where([
	        			's.srv_sv_tp_id'  => $tos,
	        			's.srv_is_active' => $surveyIsActive,
	        			'srv_pjt_id'      => $project,
	        			'srv_cnl_id'      => $channel,
	        		]);

		        if (Yii::$app->user->identity->isAdmin()) {
		        	$publisherQuery->joinWith('surveyModels AS s', false, 'INNER JOIN');
		        } else {
		        	$publisherQuery->joinWith([
		        			'surveyModels AS s' => function($q) {
		        				$q->joinWith('userSurveyRelations AS cms', false, 'INNER JOIN');
		        			}
		        		], false, 'INNER JOIN')
		        		->andWhere([
		        			'cms.usr_usr_id'  => $userId,
		        		]);
		        }
		        $publisherModels = $publisherQuery->all();

		        $answer = ['0' => '--'];

		        if (!empty($publisherModels)) {
		            foreach ($publisherModels as $publisherModel) {
		                $answer[$publisherModel->pbs_id] = $publisherModel->pbs_name;
		                if (!empty($_SESSION['remoteChained']['publisher']) && $_SESSION['remoteChained']['publisher'] == $publisherModel->pbs_id) {
		                    $publisherSel = $publisherModel->pbs_id;
		                }
		            }

		            $answer['selected'] = $publisherSel ?? (count($publisherModels) == 1 ? $publisherModels[0]->pbs_id : "0");
		            $_SESSION['remoteChained']['publisher'] = $answer['selected'];
		        }

		        $_SESSION['remoteChained']['tos'] = $tos;
		        $_SESSION['remoteChained']['project'] = $project;
		        $_SESSION['remoteChained']['channel'] = $channel;

		        return json_encode($answer);
		    } elseif($tos && $project) {
		        $channelQuery = Channel::find()
	        		->select('DISTINCT(cnl_id), cnl_name')
	        		->where([
	        			's.srv_sv_tp_id'  => $tos,
	        			's.srv_is_active' => $surveyIsActive,
	        			'srv_pjt_id'      => $project,
	        		]);

		        if (Yii::$app->user->identity->isAdmin()) {
		        	$channelQuery->joinWith('surveyModels AS s', false, 'INNER JOIN');
		        } else {
		        	$channelQuery->joinWith([
		        			'surveyModels AS s' => function($q) {
		        				$q->joinWith('userSurveyRelations AS cms', false, 'INNER JOIN');
		        			}
		        		], false, 'INNER JOIN')
		        		->andWhere([
		        			'cms.usr_usr_id'  => $userId,
		        		]);
		        }
		        $channelModels = $channelQuery->all();

		        $answer = ['0' => '--'];

		        if (!empty($channelModels)) {
		            foreach ($channelModels as $channelModel) {
		                $answer[$channelModel->cnl_id] = $channelModel->cnl_name;
		                if (!empty($_SESSION['remoteChained']['channel']) && $_SESSION['remoteChained']['channel'] == $channelModel->cnl_id) {
		                    $channelSel = $channelModel->cnl_id;
		                }
		            }

		            $answer['selected'] = $channelSel ?? (count($channelModels) == 1 ? $channelModels[0]->cnl_id : "0");
		            $_SESSION['remoteChained']['channel'] = $answer['selected'];
		        }

		        $_SESSION['remoteChained']['tos'] = $tos;
		        $_SESSION['remoteChained']['project'] = $project;

		        return json_encode($answer);
		    } elseif ($tos) {
				$projectQuery = Project::find()
	        		->select('DISTINCT(pjt_id), pjt_name, pjt_type')
	        		->where([
	        			's.srv_sv_tp_id'  => $tos,
	        			's.srv_is_active' => $surveyIsActive,
	        		]);

		        if (Yii::$app->user->identity->isAdmin()) {
		        	$projectQuery->joinWith('surveyModels AS s', false, 'INNER JOIN');
		        } else {
		        	$projectQuery->joinWith([
		        			'surveyModels AS s' => function($q) {
		        				$q->joinWith('userSurveyRelations AS cms', false, 'INNER JOIN');
		        			}
		        		], false, 'INNER JOIN')
		        		->andWhere([
		        			'cms.usr_usr_id'  => $userId,
		        		]);
		        }
		        $projectModels = $projectQuery->all();

		        $answer = ['0' => '--'];

		        if (!empty($projectModels)) {
		            foreach ($projectModels as $projectModel) {
		                $answer[$projectModel->pjt_id] = $projectModel->pjt_name . ' (' . mb_strtoupper($projectModel->pjt_type) . ')';
		                if (!empty($_SESSION['remoteChained']['project']) && $_SESSION['remoteChained']['project'] == $projectModel->pjt_id) {
		                    $projectSel = $projectModel->pjt_id;
		                }
		            }

		            $answer['selected'] = $projectSel ?? (count($projectModels) == 1 ? $projectModels[0]->pjt_id : "0");
		            $_SESSION['remoteChained']['project'] = $answer['selected'];
		        }

		        $_SESSION['remoteChained']['tos'] = $tos;

		        return json_encode($answer);
		    } else {
		    	return json_encode(['0' => '--']);
		    }
		}
	}

	public function actionGameName() 
	{
		if(Yii::$app->request->isAjax) {

		    $projectName = '';
		    $ptProjectId = Yii::$app->request->post('project_id');
		    $ptLanguageId = Yii::$app->request->post('language_id');

		    if ($ptProjectId && $ptLanguageId) {		   
		        $projectTranslation = ProjectTranslation::find()
		        	->select('pj_tr_name')
		        	->where([
		        		'pj_tr_pjt_id' => $ptProjectId, 
		        		'pj_tr_lng_id' => $ptLanguageId
		        	])->one();

		        if (!empty($projectTranslation)) {
		            $projectName = $projectTranslation->pj_tr_name;
		        }
		    }

		    return json_encode([
		    	'project_name' => $projectName
		    ]);
		}		
	}

	public function actionSurAdd() 
	{
		$get = Yii::$app->request->get();
		$post = Yii::$app->request->post();

		if(Yii::$app->request->isAjax) {
		    if (isset($post) && !empty($post)) {
		        $surveyTypeId = isset($post['tos']) ? $post['tos'] : '';
		        $surveyProjectId = isset($post['project']) ? $post['project'] : '';
		        $surveyPublisherId = isset($post['publisher']) ? $post['publisher'] : '';
		        $surveyChannelId = isset($post['channel']) ? $post['channel'] : '';
		        $surveyLanguageId = isset($post['language']) ? $post['language'] : '';

		        $survey = SurveyModel::find()
		        	->select('srv_id, srv_pjt_id')
		        	->joinWith('project', true, 'INNER JOIN')
		        	->where([
		        		'srv_sv_tp_id' => $surveyTypeId,
		        		'srv_pjt_id'   => $surveyProjectId,
		        		'srv_pbs_id'   => $surveyPublisherId,
		        		'srv_cnl_id'   => $surveyChannelId,
		        		'srv_lng_id'   => $surveyLanguageId,
		        	])->one();

		        if (empty($survey)) {
		            return json_encode([
		            	'status' => 'error', 
		            	'message' => 'no survey for params',
		            ]);
		        } else {
		            $linkLinkCode = Link::getCode();
		            $linkModel = new Link();
		            $linkData = [
						'lnk_srv_id'     => $survey->srv_id,
						'lnk_code'       => $linkLinkCode,
						'lnk_usr_id'     => Yii::$app->user->identity->usr_id,
						'lnk_creator'    => Yii::$app->user->identity->usr_login,
						'lnk_comment'    => $post['comment'],
						'lnk_is_active'  => 1,
						'lnk_session_id' => session_id(),
						'lnk_dateline'   => time(),
						'lnk_lnk_tp_id'  => 1,
		            ];
		            $linkModel->attributes = $linkData;
		            $linkModel->save();

		            if (is_array($post['custom'])) {
		                foreach ($post['custom'] as $key => $value) {

		                	$questionCustomValueModel = new QuestionCustomValue();

		                    switch ($key) {
		                        case 'game':
		                            $customQId = 1;
		                            break;
		                        case 'event':
		                            $customQId = 5;
		                            break;
		                        case 'activity':
		                            $customQId = 13;
		                            break;
		                    }

		                    if ($survey->project->pjt_type == 'ru') {
		                        $value = '"' . $value . '"';
		                    }

		                    $questionCustomValueData = [
								'ct_vl_code'   => $key,
								'ct_vl_value'  => $value,
								'ct_vl_srv_id' => $survey->srv_id,
								'ct_vl_qst_id' => $customQId,
								'ct_vl_lnk_id' => $linkModel->lnk_id,
		                    ];

		                    $questionCustomValueModel->attributes = $questionCustomValueData;
		                    $questionCustomValueModel->save();
		                }
		            }

		            $forumUrl = str_replace(
						'{link}', Tools::generateLink($linkLinkCode),
		                '[URL="{link}"]' . (str_replace(': {link}', '',
		                Link::$surLinkText[$surveyTypeId][$surveyLanguageId])) . '[/URL]'
					);

		            $urlText = str_replace(
						'{link}', 
						Tools::generateLink($linkLinkCode),
		                Link::$surLinkText[$surveyTypeId][$surveyLanguageId]
					);

		            $urlLinkText = '<a href="' . Tools::generateLink($linkLinkCode) . '">' . str_replace([': {link}','{link}'],
		                '', Link::$surLinkText[$surveyTypeId][$surveyLanguageId]) . '</a>';

		            return json_encode([
		               'status'        => 'success',
		               'message'       => 'Ссылка добавлена в БД',
		               'url'           => Tools::generateLink($linkLinkCode),
		               'forum_url'     => $forumUrl,
		               'url_text'      => $urlText,
		               'url_link_text' => $urlLinkText,
		            ]);

		        }
		    }
		}		
	}

	public function actionSurCreate() 
	{
		$get = Yii::$app->request->get();
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {
		    if (isset($post) && !empty($post)) {

		        $surveyTypeId = isset($post['tos']) ? $post['tos'] : '';
		        $surveyProjectId = isset($post['project']) ? $post['project'] : '';
		        $surveyPublisherId = isset($post['publisher']) ? $post['publisher'] : '';
		        $surveyChannelId = isset($post['channel']) ? $post['channel'] : '';
		        $surveyLanguageId = isset($post['language']) ? $post['language'] : '';
		        $linkIsActive = 1;

		        $surveyModel = new SurveyModel();
		        $surveyData = [
					'srv_sv_tp_id'   => $surveyTypeId,
					'srv_pjt_id'     => $surveyProjectId,
					'srv_pbs_id'     => $surveyPublisherId,
					'srv_cnl_id'     => $surveyChannelId,
					'srv_lng_id'     => $surveyLanguageId,
					'srv_is_active'  => $linkIsActive,
		        ];
		        $surveyModel->attributes = $surveyData;
		        $surveyModel->save();

		        if (empty($surveyModel->srv_id)) {
		            return json_encode([
		               'status'  => 'success',
		               'message' => 'дубль'
		            ]);
		        } else {
		        	$questions = Question::find()
		        		->select('qst_id')
		        		->where([
		        			'qst_srv_tp_id' => $surveyTypeId
		        		])->all();

		            foreach ($questions as $question) {
		            	$questionSurbeyRelation = new QuestionSurbeyRelation();
		            	$questionSurbeyRelation->attributes = [
		            		'qsr_srv_id' => $surveyModel->srv_id,
		            		'qsr_qst_id' => $question->qst_id,
		            	];
		            	$questionSurbeyRelation->save();
		            }

		            $surveyTemplates = SurveyTemplate::find()
		            	->select('sv_tm_code, sv_tm_thank_you, sv_tm_sv_t_t_id, sv_tm_srv_id')
		            	->joinWith('surveyModel s',false, 'INNER JOIN')
		            	->where([
		            		's.srv_pjt_id' => $surveyProjectId,
		            		's.srv_lng_id' => $surveyLanguageId,
		            	])->limit(2)
		            	->all();

		            $svTmProbability = 50;
		            foreach ($surveyTemplates as $surveyTemplate) {
		            	$surveyTemplateModel = new SurveyTemplate();
		            	$surveyTemplateData = [
							'sv_tm_srv_id'       => $surveyModel->srv_id,
							'sv_tm_code'         => $surveyTemplate->sv_tm_code,
							'sv_tm_thank_you'    => $surveyTemplate->sv_tm_thank_you,
							'sv_tm_probability'  => $svTmProbability,
							'sv_tm_sv_t_t_id'    => $surveyTemplate->sv_tm_sv_t_t_id,
		            	];
		            	$surveyTemplateModel->attributes = $surveyTemplateData;
		            	$surveyTemplateModel->save();
		            }
		        }

		        return json_encode([
		           'status'  => 'success',
		           'message' => 'Ссылка добавлена в БД' . $surveyModel->srv_id
		        ]);

		    }
		}
	}
}