<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\sqhelp\{Link, SurveyAnswer, SurveySession, SurveyType, SurveyModel, ProjectTranslation, QuestionCustomValue, HelpdeskDepartmentId, HelpdeskArticleCategoryId};
use app\models\cscommon\{Project, Channel, Language, Publisher, User};
use app\components\{Tools};

class ApiController extends Controller
{
    public function actionGetInfo() 
    {
        $hashs = ['so9ru1leet6BahHaizugh7Shuamu3u'];
        $linkCode = Yii::$app->request->get('link');
        $hash = Yii::$app->request->get('hash');
        if($linkCode && in_array($hash, $hashs)) {
            $link = Link::find()
                ->where([
                    'lnk_code' => $linkCode,
                ])->one();
            if(!$link) {
                return 'bad code';
            }

            $result = [
                'LINK'        => [
                    'id'           => $link->lnk_id,
                    'code'         => $link->lnk_code,
                    'creator_id'   => $link->lnk_usr_id,
                    'creator_name' => $link->lnk_creator,
                    'comment'      => $link->lnk_comment,
                    'is_active'    => $link->lnk_is_active,
                    'creator_uid'  => $link->lnk_session_id,
                    'dateline'     => $link->lnk_dateline,
                    'create_time'  => date('H:i:s d.m.Y', $link->lnk_dateline)
                ],
                'SURVEY_TYPE' => [
                    'id'   => $link->surveyModel->surveyType->sv_tp_id,
                    'code' => $link->surveyModel->surveyType->sv_tp_code,
                    'name' => $link->surveyModel->surveyType->sv_tp_name
                ],
                'PROJECT'     => [
                    'id'   => $link->surveyModel->project->pjt_id,
                    'code' => $link->surveyModel->project->pjt_code,
                    'name' => $link->surveyModel->project->pjt_name,
                    'type' => $link->surveyModel->project->pjt_type
                ],
                'PUBLISHER'   => [
                    'id'   => $link->surveyModel->publisher->pbs_id,
                    'code' => $link->surveyModel->publisher->pbs_code,
                    'name' => $link->surveyModel->publisher->pbs_name
                ],
                'PLATFORM'    => [
                    'id'   => $link->surveyModel->publisher->platform->ptm_id,
                    'code' => $link->surveyModel->publisher->platform->ptm_code,
                    'name' => $link->surveyModel->publisher->platform->ptm_name
                ],
                'CHANNEL'     => [
                    'id'   => $link->surveyModel->channel->cnl_id,
                    'code' => $link->surveyModel->channel->cnl_code,
                    'name' => $link->surveyModel->channel->cnl_name
                ],
                'LANGUAGE'    => [
                    'id'   => $link->surveyModel->language->lng_id,
                    'code' => $link->surveyModel->language->lng_code,
                    'name' => $link->surveyModel->language->lng_name
                ]
            ];

            $answers = SurveyAnswer::find()
                ->where([
                    'us_rs_lnk_id' => $link->lnk_id
                ])->orderBy('us_rs_an_tp_id ASC, us_rs_dateline ASC')
                ->all();

            foreach($answers as $answer) {
                $result['ANSWERS'][date('d.m.Y H:i:s',
                    $answer->us_rs_dateline)][$answer->us_rs_form_code][$answer->question->qst_code]['quest'] = $answer->question->qst_text;

                switch ($answer->us_rs_an_tp_id) {
                    case 1:
                        $result['ANSWERS'][date('d.m.Y H:i:s',
                            $answer->us_rs_dateline)][$answer->us_rs_form_code][$answer->question->qst_code]['star'] = $answer->us_rs_value;
                        break;

                    case 2:
                        $result['ANSWERS'][date('d.m.Y H:i:s',
                            $answer->us_rs_dateline)][$answer->us_rs_form_code][$answer->question->qst_code]['presets'][$answer->answerPreset->ans_code] = $answer->answerPreset->ans_text;
                        break;

                    case 3:
                        if (!$answer->answerPreset && empty($answer->answerPreset->ans_code)) {
                            $result['ANSWERS'][date('d.m.Y H:i:s',
                                $answer->us_rs_dateline)][$answer->us_rs_form_code][$answer->question->qst_code]['presets']['comment'] = $answer->us_rs_value;
                        }
                        break;

                    case 4:
                        $result['ANSWERS'][date('d.m.Y H:i:s',
                            $answer->us_rs_dateline)][$answer->us_rs_form_code][$answer->question->qst_code]['comment'] = $answer->us_rs_value;
                        break;
                }                
            }

            $surveySessionCount = SurveySession::find()
                ->select('us_vt_session_id')
                ->distinct()
                ->where([
                    'us_vt_lnk_id' => $link->lnk_id
                ])->count();

            $result['SEEN'] = $surveySessionCount ?? 0;
            return json_encode($result);
        }
    }

    public function actionSurveyCreate() {
        if (!Yii::$app->request->post()) {
            $post = json_decode(file_get_contents("php://input"), true) ? : array();
        } else {
            $post = Yii::$app->request->post();
        }    

        $post = [
            // required parameters
            'survey_type'    => 'support',
            'project_code'   => 'MERU',
            'publisher_code' => 'GP',
            'channel_code'   => 'GP',
            'language_code'  => 'ru',
            'staff_login'    => 't.kovalev',
            'access_token'   => sha1('letmeindude'),

            // optional parameters
            'comment'        => 'Look I\'m the comment',
            'custom'         => [
                'event' => 'Awesome event name'
            ]
        ]; 

        if (!$post || empty($post['access_token']) || $post['access_token'] !== sha1('letmeindude')) {
            return $this->jsonResponse(400 , 'Bad or empty POST parameters'); 
        }

        if (!empty($post['survey_type']) && !empty(SurveyType::$surveyIds[strtolower($post['survey_type'])])) {
            $surveyTypeId = SurveyType::$surveyIds[strtolower($post['survey_type'])];
        } else {
            return $this->jsonResponse(400 , 'Bad or empty survey type'); 
        }

        // Project
        $projectCode = $post['project_code'] ?? '';
        $project = Project::find()
            ->select('pjt_id')
            ->where(['pjt_code' => $projectCode])
            ->one();
        $projectId = $project->pjt_id;
        
        if (!$projectId) {
            return $this->jsonResponse(400, 'Bad or empty project code');
        }

        // Publisher
        $publisherCode = $post['publisher_code'] ?? '';
        $publisher = Publisher::find()
            ->select('pbs_id')
            ->where(['pbs_code' => $publisherCode])
            ->one();
        $publisherId = $publisher->pbs_id;

        if (!$publisherId) {
            return $this->jsonResponse(400, 'Bad or empty publisher code');
        }

        // Channel
        $channelCode = $post['channel_code'] ?? '';
        $channel = Channel::find()
            ->select('cnl_id')
            ->where(['cnl_code' => $channelCode])
            ->one();
        $channelId = $channel->cnl_id;

        if (!$channelId) {
            return $this->jsonResponse(400, 'Bad or empty channel code');
        }

        // Language
        $languageCode = $post['language_code'] ?? '';
        // Термоядерный костыль, т.к. двухбуквенный код японского языка - ja, а не jp (ISO_639-1)
        $languageCode = 'ja' === $languageCode ? 'jp' : $languageCode;
        // для китайского zh -> zhs
        $languageCode = 'zh' === $languageCode ? 'zhs' : $languageCode;

        $language = Language::find()
            ->select('lng_id')
            ->where(['lng_code' => $languageCode])
            ->one();
        $languageId = $language->lng_id;

        if (!$languageId) {
            return $this->jsonResponse(400, 'Bad or empty language code');
        }

        // Staff
        $staffLogin = $post['staff_login'] ?? '';
        $staff = User::find()
            ->select('usr_id, usr_login')
            ->where(['usr_login' => $staffLogin])
            ->one();

        if (empty($staff->usr_id)) {
            return $this->jsonResponse(400, 'Bad or empty staff login');
        }

        $surveyModel = SurveyModel::find()
            ->select('srv_id, srv_pjt_id')
            ->joinWith(['project p' => function($q) {
                $q->select('pjt_type');
            }], true, 'INNER JOIN')
            ->where([
                'srv_sv_tp_id' => $surveyTypeId,
                'srv_pjt_id' => $projectId,
                'srv_pbs_id' => $publisherId,
                'srv_cnl_id' => $channelId,
                'srv_lng_id' => $languageId,
            ])->one();

        if(!$surveyModel) {
            return $this->jsonResponse(400, 'There is no Survey Model for request parameters');
        }

        $linkCode = Link::getCode();
        $comment = $post['comment'] ?? '';

        $linkModel = new Link();
        $linkData = [
            'lnk_srv_id'     => $surveyModel->srv_id,
            'lnk_code'       => $linkCode,
            'lnk_usr_id'     => $staff->usr_id,
            'lnk_creator'    => $staff->usr_login,
            'lnk_comment'    => $comment,
            'lnk_is_active'  => 1,
            'lnk_session_id' => null,
            'lnk_dateline'   => time(),
            'lnk_lnk_tp_id'  => 1,
        ];
        $linkModel->attributes = $linkData;
        $linkModel->save();

        if ($surveyTypeId == 1) {
            $projectTranslation = ProjectTranslation::find()
                ->select('pj_tr_name')
                ->where([
                    'pj_tr_pjt_id' => $projectId,
                    'pj_tr_lng_id' => $languageId,
                ])->one();

            if (!empty($projectTranslation->pj_tr_name)) {
                $post['custom']['game'] = $projectTranslation->pj_tr_name;
            }
        }

        if (!empty($post['custom']) && is_array($post['custom'])) {
            foreach ($post['custom'] as $key => $value) {
                switch ($key) {
                    case 'game':
                        $customQuestionId = 1;
                        break;

                    case 'event':
                        $customQuestionId = 5;
                        break;

                    case 'activity':
                        $customQuestionId = 13;
                        break;

                    default:
                        $customQuestionId = null;
                }

                if (!empty($customQuestionId) && !empty($value)) {
                    if ($surveyModel->project->pjt_type == 'ru') {
                        $value = '"' . $value . '"';
                    }

                    $questionCustomValueModel = new QuestionCustomValue();
                    $questionCustomValueData = [
                        'ct_vl_code'   => $key,
                        'ct_vl_value'  => $value,
                        'ct_vl_srv_id' => $surveyModel->srv_id,
                        'ct_vl_qst_id' => $customQuestionId,
                        'ct_vl_lnk_id' => $linkModel->lnk_id,
                    ];

                    $questionCustomValueModel->attributes = $questionCustomValueData;
                    $questionCustomValueModel->save();
                }
            }
        }

        $url = Tools::generateLink($linkCode);

        $response = [
            'data' => [
                'url'           => $url,
                'url_text'      => str_replace(
                    '{link}',
                    $url,
                    Link::$surLinkText[$surveyTypeId][$languageId]
                ),
                'forum_url'     => str_replace(
                    '{link}',
                    $url,
                    '[URL="{link}"]' . (str_replace(': {link}', '',
                        Link::$surLinkText[$surveyTypeId][$languageId])) . '[/URL]'
                ),
                'url_link_text' => '<a href="' . $url . '">' . str_replace(
                        [
                            ': {link}',
                            '{link}'
                        ],
                        '',
                        Link::$surLinkText[$surveyTypeId][$languageId]
                    ) . '</a>'
            ]
        ];

        return $this->jsonResponse(200, $response);
        
    }

    public function jsonResponse($code = 200, $message = null) {
        http_response_code($code);
        $status = [
            200 => '200 OK',
            400 => '400 Bad Request',
            418 => 'I\'m a teapot',
            500 => '500 Internal Server Error'
        ];
        header('Status: ' . $status[$code]);

        return json_encode([
            'status'  => $code < 300,
            'message' => $message,
        ]);
    }
}